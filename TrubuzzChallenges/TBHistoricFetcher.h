//
//  TBHistoricFetcher.h
//  TrubuzzChallenges
//
//  Created by Taras Vozniuk on 5/7/15.
//  Copyright (c) 2015 ambientlight. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    TBHistoricQuotePeriodDaily,
    TBHistoricQuotePeriodHour,
    TBHistoricQuotePeriod30Min,
    TBHistoricQuotePeriod15Min,
    TBHistoricQuotePeriod5Min,
    TBHistoricQuotePeriod1Min
    
} TBHistoricQuotePeriod;

@interface TBHistoricFetcher : NSObject

+ (void)fetchQuotesForCountry:(NSString*)country
                       symbol:(NSString*)symbol
                     exchange:(NSString*)exchange
                     barCount:(NSUInteger)lastBarsCount
                       period:(TBHistoricQuotePeriod)period
         andCompletionHandler:(void (^)(NSArray* quotes))completionHandler;

 
@end
