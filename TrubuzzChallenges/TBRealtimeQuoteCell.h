//
//  TBRealtimeQuoteCell.h
//  TrubuzzChallenges
//
//  Created by Taras Vozniuk on 5/8/15.
//  Copyright (c) 2015 ambientlight. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TBRealtimeQuoteCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *symbolLabel;
@property (weak, nonatomic) IBOutlet UIImageView *upDownImageVIew;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (nonatomic, strong) NSString* identifier;

@end
