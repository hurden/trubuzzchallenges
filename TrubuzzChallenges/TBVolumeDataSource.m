//
//  TBVolumeDataSource.m
//  TrubuzzChallenges
//
//  Created by Taras Vozniuk on 5/8/15.
//  Copyright (c) 2015 ambientlight. All rights reserved.
//

#import "TBVolumeDataSource.h"
#import "TBHistoricQuote.h"

@interface TBVolumeDataSource()

@property (nonatomic, strong) NSMutableArray* volumeSeries;

@end

@implementation TBVolumeDataSource {
    NSUInteger _refreshCount;
    NSTimer* _refreshTimer;
}

- (instancetype)initWithQuotes:(NSArray*)quotes {
    
    self = [super init];
    if (self) {
        
        _volumeSeries = [NSMutableArray array];
        //volume
        for (TBHistoricQuote* quote in quotes) {
            
            SChartDataPoint* datapoint = [[SChartDataPoint alloc] init];
            datapoint.xValue = quote.date;
            
            // excuse me for this
            datapoint.yValue = @(quote.volume / 1000000);
            
            [_volumeSeries addObject:datapoint];
        }
        
    }
    
    return self;
}

#pragma mark - SChartDatasource

- (NSInteger)numberOfSeriesInSChart:(ShinobiChart *)chart {
    return 1;
}

-(SChartSeries *)sChart:(ShinobiChart *)chart seriesAtIndex:(NSInteger)index {
    SChartColumnSeries *columnSeries = [SChartColumnSeries new];
    columnSeries.style.areaColor = UIColor.blueColor;
    return columnSeries;
}

- (NSInteger)sChart:(ShinobiChart *)chart numberOfDataPointsForSeriesAtIndex:(NSInteger)seriesIndex {
    return self.volumeSeries.count;
}

- (id<SChartData>)sChart:(ShinobiChart *)chart dataPointAtIndex:(NSInteger)dataIndex forSeriesAtIndex:(NSInteger)seriesIndex {
    return self.volumeSeries[dataIndex];
}

#pragma mark - SChartDelegate

- (void)sChartIsPanning:(ShinobiChart *)chart {
    
    [self syncronizeMainChartFromChart:chart];
}

- (void)sChartDidFinishPanning:(ShinobiChart *)chart
{
   // NSLog(@"didFinishPanning volumeChart");
    _refreshCount = 50;
    
    // workaround to syncronize charts after panning is finished but chart still moves, since panning was done
    // on the border of the data series
    // refresh on 50Frames per sec (0.02 of a sec)
    
    // still don't forget to add condition to check for a dataRange end, don't resynch in the middle of dataRange
    
    _refreshTimer = [NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:@selector(handleRefresh:) userInfo:chart repeats:YES];
}

- (void)handleRefresh:(NSTimer*)timer
{
    [self syncronizeMainChartFromChart:timer.userInfo];
    _refreshCount--;
    
    if (_refreshCount == 0) {
        [_refreshTimer invalidate];
    }
}

- (void)sChartIsZooming:(ShinobiChart *)chart {
    
    [self syncronizeMainChartFromChart:chart];
}

// This method updates all charts in the _managedCharts array with the chart's axis ranges passed in as the parameter.

- (void)syncronizeMainChartFromChart:(ShinobiChart* )chart {
    
    SChartRange *zoomedXRange = chart.xAxis.axisRange;
    
    [self.mainChart.xAxis setRangeWithMinimum:zoomedXRange.minimum andMaximum:zoomedXRange.maximum];
    [self.mainChart redrawChart];
}


@end
