//
//  TBCandlestickSeries.h
//  TrubuzzChallenges
//
//  Created by Taras Vozniuk on 5/8/15.
//  Copyright (c) 2015 ambientlight. All rights reserved.
//

#import <ShinobiCharts/ShinobiCharts.h>

@interface TBCandlestickSeries : SChartCandlestickSeries

+ (instancetype)defaultSeries;

@end
