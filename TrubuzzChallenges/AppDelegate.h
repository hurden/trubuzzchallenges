//
//  AppDelegate.h
//  TrubuzzChallenges
//
//  Created by Taras Vozniuk on 5/6/15.
//  Copyright (c) 2015 ambientlight. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

