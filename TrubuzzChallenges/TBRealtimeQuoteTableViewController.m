//
//  TBRealtimeQuoteTableViewController.m
//  TrubuzzChallenges
//
//  Created by Taras Vozniuk on 5/8/15.
//  Copyright (c) 2015 ambientlight. All rights reserved.
//

#import "TBRealtimeQuoteTableViewController.h"
#import "TBRealtimeFetcher.h"
#import "TBRealtimeQuoteCell.h"

@interface TBRealtimeQuoteTableViewController()

@property (nonatomic, strong) TBRealtimeFetcher* fetcher;

@end

@implementation TBRealtimeQuoteTableViewController


- (void)viewDidLoad {
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
    self.navigationItem.titleView = imageView;
    
    
    self.fetcher = [TBRealtimeFetcher fetcher];
    [self.fetcher openConnectionWithCompletionHandler:^(bool didOpen){
        
        if (didOpen){
            [self.fetcher subscribeToDemoTrubuzzSymbolsWithDelegate:self];
        }
        
    }];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [self.fetcher closeConnection];
}

#pragma mark - UITableViewDelegate && UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fetcher.symbols.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TBRealtimeQuoteCell* cell = [self.tableView dequeueReusableCellWithIdentifier:@"rtCell" forIndexPath:indexPath];
    cell.symbolLabel.text = [self.fetcher.symbols[indexPath.row] substringFromIndex:11];
    cell.priceLabel.text = @"Waiting...";
    cell.identifier = self.fetcher.symbols[indexPath.row];
    
    return cell;
}

#pragma mark - LSTableDelegate

- (void) table:(LSSubscribedTableKey *)tableKey itemPosition:(int)itemPosition itemName:(NSString *)itemName didUpdateWithInfo:(LSUpdateInfo *)updateInfo
{
    TBRealtimeQuoteCell* foundCell = nil;
    for (TBRealtimeQuoteCell* cell in self.tableView.visibleCells) {
        if ([cell.identifier isEqualToString:itemName]) {
            foundCell = cell;
        }
    }
    
    if (!foundCell) {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        foundCell.priceLabel.text = [NSString stringWithFormat:@"%@",
                                    [updateInfo currentValueOfFieldName:@"Price"] ];
        
        if ([updateInfo currentValueOfFieldName:@"Price"].doubleValue > [updateInfo previousValueOfFieldName:@"Price"].doubleValue) {
            foundCell.upDownImageVIew.image = [UIImage imageNamed:@"Dark_Green_Arrow_Up.png"];
        } else {
            foundCell.upDownImageVIew.image = [UIImage imageNamed:@"Red_Arrow_Down.png"];
        }
    });
    
    NSLog(@"UPDATE:");
    NSLog(@"Price: %@", [updateInfo currentValueOfFieldName:@"Price"]);
    NSLog(@"Symbol: %@", [updateInfo currentValueOfFieldName:@"Offer"]);
    NSLog(@"Offer: %@", [updateInfo currentValueOfFieldName:@"NetChgPrevDDay"]);
    NSLog(@"UPDATE END");
}

@end
