//
//  TBFetcher.h
//  TrubuzzChallenges
//
//  Created by Taras Vozniuk on 5/7/15.
//  Copyright (c) 2015 ambientlight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Lightstreamer.h"

@interface TBRealtimeFetcher : NSObject <LSConnectionDelegate>

+ (instancetype) fetcher;
- (void)openConnectionWithCompletionHandler:(void (^)(bool))completionHandler;
- (void)subscribeToDemoTrubuzzSymbolsWithDelegate:(id<LSTableDelegate>)delegate;

- (void)closeConnection;

- (NSArray*)symbols;

@end
