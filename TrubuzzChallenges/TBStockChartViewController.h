//
//  TBStockChartViewController.h
//  TrubuzzChallenges
//
//  Created by Taras Vozniuk on 5/8/15.
//  Copyright (c) 2015 ambientlight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ShinobiCharts/ShinobiCharts.h>

@interface TBStockChartViewController : UIViewController <SChartDatasource, SChartDelegate>

- (instancetype)initWithQuotes:(NSArray*)quotes;

@end
