//
//  TBHistoricQuote.m
//  TrubuzzChallenges
//
//  Created by Taras Vozniuk on 5/7/15.
//  Copyright (c) 2015 ambientlight. All rights reserved.
//

#import "TBHistoricQuote.h"



@interface TBHistoricQuote()

@property (nonatomic, strong) NSDate* date;
@property (nonatomic, assign) double open;
@property (nonatomic, assign) double close;
@property (nonatomic, assign) double high;
@property (nonatomic, assign) double low;
@property (nonatomic, assign) unsigned long long volume;

@end

@implementation TBHistoricQuote

+ (instancetype)quoteFromDictionary:(NSDictionary*)quoteDict
{
    TBHistoricQuote* newQuote = [[self alloc] init];
    newQuote.open = [quoteDict[openKey] doubleValue];
    newQuote.close = [quoteDict[closeKey] doubleValue];
    newQuote.high = [quoteDict[highKey] doubleValue];
    newQuote.low = [quoteDict[lowKey] doubleValue];
    
    NSNumberFormatter* numberFormater = [[NSNumberFormatter alloc] init];
    newQuote.volume = [[numberFormater numberFromString:quoteDict[volumeKey]] unsignedLongLongValue];
    
    
    NSString* epochMicrosecondsString = quoteDict[entryTimeKey];
    
    // even if API specifies time in EPOCH Milliseconds, the values provided
    // does not contain millisecond accuracy, so safely discard milliseconds
    NSTimeInterval epochSeconds = epochMicrosecondsString.doubleValue / 1000;
    newQuote.date = [NSDate dateWithTimeIntervalSince1970:epochSeconds];
    
    // daily charts, but the hours:mins for some differ, make it the same
    // so shinobi will parse it easily
    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate* resultDate = [calendar dateBySettingHour:0 minute:0 second:0 ofDate:newQuote.date options:0];
    newQuote.date = resultDate;
    
    
    
    return newQuote;
}

@end
