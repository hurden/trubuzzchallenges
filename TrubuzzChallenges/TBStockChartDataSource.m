//
//  TBStockChartDatasource.m
//  TrubuzzChallenges
//
//  Created by Taras Vozniuk on 5/8/15.
//  Copyright (c) 2015 ambientlight. All rights reserved.
//

#import "TBStockChartDatasource.h"
#import "TBCandlestickSeries.h"
#import "TBHistoricQuote.h"

@interface TBStockChartDatasource()

@property (nonatomic, strong) NSArray* quotes;

@end

@implementation TBStockChartDatasource

+ (instancetype)datasourceWithHistoricQuotes:(NSArray*)quotes {
    
    TBStockChartDatasource* datasource = [[self alloc] init];
    datasource.quotes = quotes;
    
    return datasource;
}

#pragma mark - SChartDatasource

- (NSInteger)numberOfSeriesInSChart:(ShinobiChart *)chart {
    return 1;
}

- (SChartSeries*)sChart:(ShinobiChart *)chart seriesAtIndex:(NSInteger)index
{
    return [TBCandlestickSeries defaultSeries];
}

- (NSInteger)sChart:(ShinobiChart *)chart numberOfDataPointsForSeriesAtIndex:(NSInteger)seriesIndex {
    
    return self.quotes.count;
}

- (SChartAxis*)sChart:(ShinobiChart *)chart yAxisForSeriesAtIndex:(NSInteger)index
{
    return chart.allYAxes[0];
}

- (NSArray*)sChart:(ShinobiChart *)chart dataPointsForSeriesAtIndex:(NSInteger)seriesIndex
{
    NSMutableArray *datapoints = [NSMutableArray array];
    NSUInteger noPoints = [self sChart:chart numberOfDataPointsForSeriesAtIndex:seriesIndex];
    
    for (int i = 0; i < noPoints; i++) {
        [datapoints addObject:[self candlestickDataPointAtIndex:i]];
    }
    
    if (datapoints.count == 0) {
        datapoints = nil;
    }
    
    return datapoints;
}

- (id<SChartData>)sChart:(ShinobiChart *)chart dataPointAtIndex:(NSInteger)dataIndex forSeriesAtIndex:(NSInteger)seriesIndex
{
    return [self candlestickDataPointAtIndex:dataIndex];
}

- (id<SChartData>)candlestickDataPointAtIndex:(NSUInteger)dataIndex
{
    SChartMultiYDataPoint* dataPoint = [[SChartMultiYDataPoint alloc] init];
    
    TBHistoricQuote* currentQuote = self.quotes[dataIndex];
    dataPoint.xValue = currentQuote.date;
    
    NSDictionary* yValuesDict = @{@"Open": @(currentQuote.open),
                                  @"High": @(currentQuote.high),
                                  @"Low": @(currentQuote.low),
                                  @"Close": @(currentQuote.close)};
    
    dataPoint.yValue = [yValuesDict mutableCopy];
    return dataPoint;
}


@end
