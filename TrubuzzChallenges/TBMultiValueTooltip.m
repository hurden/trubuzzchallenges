//
//  TBMultiValueTooltip.m
//  TrubuzzChallenges
//
//  Created by Taras Vozniuk on 5/9/15.
//  Copyright (c) 2015 ambientlight. All rights reserved.
//

#import "TBMultiValueTooltip.h"

@implementation TBMultiValueTooltip

- (NSDictionary*) keyValueDisplayPairsForDataPoint:(id<SChartData>) dataPoint onSeries:(SChartSeries*) series withXAxis:(SChartAxis*) xAxis withYAxis:(SChartAxis*) yAxis
{
    SChartMultiYDataPoint* point = dataPoint;
    return @{ @"Open:": [point.yValues[SChartCandlestickKeyOpen] stringValue],
              @"High:": [point.yValues[SChartCandlestickKeyHigh] stringValue],
              @"Low:":  [point.yValues[SChartCandlestickKeyLow] stringValue],
              @"Close:": [point.yValues[SChartCandlestickKeyClose] stringValue]};
}

- (NSString*) mainLabelStringForDataPoint:(id<SChartData>) dataPoint onSeries:(SChartSeries*) series withXAxis:(SChartAxis*) xAxis withYAxis:(SChartAxis*) yAxis
{
    SChartDataPoint* point = dataPoint;
    NSDateFormatter* dataFormater = [[NSDateFormatter alloc] init];
    dataFormater.dateFormat = @"yyyy-MM-dd";
    return [dataFormater stringFromDate:point.xValue];
}

@end
