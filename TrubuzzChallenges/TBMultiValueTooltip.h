//
//  TBMultiValueTooltip.h
//  TrubuzzChallenges
//
//  Created by Taras Vozniuk on 5/9/15.
//  Copyright (c) 2015 ambientlight. All rights reserved.
//

#import <ShinobiCharts/ShinobiCharts.h>

@interface TBMultiValueTooltip : SChartCrosshairMultiValueTooltip

@end
