//
//  TBHistoricFetcher.m
//  TrubuzzChallenges
//
//  Created by Taras Vozniuk on 5/7/15.
//  Copyright (c) 2015 ambientlight. All rights reserved.
//

#import "TBHistoricFetcher.h"
#import "TBHistoricQuote.h"

#define jsonFetchURLPath @"https://guruapi.trubuzz.cn/api/marketdata/histories/?"

static dispatch_queue_t __historicQoutesFetchQueue = nil;



@implementation TBHistoricFetcher

+ (void)fetchQuotesForCountry:(NSString*)country
                       symbol:(NSString*)symbol
                     exchange:(NSString*)exchange
                     barCount:(NSUInteger)lastBarsCount
                       period:(TBHistoricQuotePeriod)period
         andCompletionHandler:(void (^)(NSArray* quotes))completionHandler
{
    if (!__historicQoutesFetchQueue) {
        __historicQoutesFetchQueue = dispatch_queue_create("com.trubuzzChallenges.historicFetchQueue", dispatch_queue_attr_make_with_qos_class(DISPATCH_QUEUE_SERIAL, QOS_CLASS_BACKGROUND, 0));
    }
    
    NSString* fetchURLString = [NSString stringWithFormat:@"%@country=%@&symbol=%@&exchange=%@&pageSize=%ld&period=%@", jsonFetchURLPath, country, symbol, exchange, lastBarsCount, [self stringFromQuotePeriod:period]];
    
    
    dispatch_async(__historicQoutesFetchQueue, ^{
    
        NSData* jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString: fetchURLString]];
        if (!jsonData) {
            NSLog(@"Couldn't fetch data");
            
            if (completionHandler)
                completionHandler(nil);
        } else {
            
            NSError* parsingError = nil;
            NSArray* historicQuotesArray = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:&parsingError];
            if (parsingError){
                NSLog(@"parsing error");
                
                if (completionHandler)
                    completionHandler(nil);
            } else {
                
                NSMutableArray* quotes = [NSMutableArray array];
                for (NSDictionary* quoteDictionary in historicQuotesArray) {
                    
                    TBHistoricQuote* quote = [TBHistoricQuote quoteFromDictionary:quoteDictionary];
                    //ignore quotes with 0 volume (bad data)
                    if (quote.volume > 0) {
                        [quotes addObject:quote];
                    }
                }
                
                if (completionHandler)
                    completionHandler(quotes);
                
            }
        }
        
        
    
    });
    
}


+ (NSString*)stringFromQuotePeriod:(TBHistoricQuotePeriod)period {
    
    switch (period) {
        case TBHistoricQuotePeriodDaily: return @"1day";
        case TBHistoricQuotePeriodHour:  return @"1hour";
        case TBHistoricQuotePeriod30Min: return @"30min";
        case TBHistoricQuotePeriod15Min: return @"15min";
        case TBHistoricQuotePeriod5Min:  return @"5min";
        case TBHistoricQuotePeriod1Min:  return @"1min";
            
        default: return @"1day";
    }
    
}
                                
@end
