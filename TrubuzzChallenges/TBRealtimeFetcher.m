//
//  TBFetcher.m
//  TrubuzzChallenges
//
//  Created by Taras Vozniuk on 5/7/15.
//  Copyright (c) 2015 ambientlight. All rights reserved.
//

#import "TBRealtimeFetcher.h"

#define trubuzzPushServerURL @"http://54.241.5.104:80/"
#define trubuzzConnectionAdapterName @"MarketChinaAdapter"
#define trubuzzTableItems @[@"China.CCFX.SFIF0001", @"China.CCFX.SFIF0002"]
#define trubuzzTableFields @[@"Price", @"Offer", @"NetChgPrevDDay", @"Bid", @"FirstPx", @"PrevClosePx", @"TotalVolumeTraded", @"TradeVolume", @"MDEntryTime", @"HighPx", @"LowPx"]
#define trubuzzDataAdapter @"MarketChinaAdapter_SF"



#define defaultServerURL @"http://push.lightstreamer.com"
#define defaultConnectionAdapterName @"DEMO"
#define defaultTableItems @[@"item1", @"item2", @"item3", @"item4", @"item5", @"item6", @"item7", @"item8", @"item9", @"item10", @"item11", @"item12", @"item13", @"item14", @"item15", @"item16", @"item17", @"item18", @"item19", @"item20", @"item21", @"item22", @"item23", @"item24", @"item25", @"item26", @"item27", @"item28", @"item29", @"item30"]
#define defaultTableFields @[@"last_price", @"time", @"pct_change", @"stock_name"]
#define defaultDataAdapter @"QUOTE_ADAPTER"


@interface TBRealtimeFetcher()

@property (nonatomic, strong) LSClient* client;
@property (nonatomic, strong) LSConnectionInfo* connectionInfo;
@property (nonatomic, strong) LSSubscribedTableKey* subscriptionKey;
@property (nonatomic, strong) dispatch_queue_t fetchQueue;


@end

@implementation TBRealtimeFetcher {
    void (^_connectionHandler)(bool);
}

+ (instancetype) fetcher {

    TBRealtimeFetcher* fetcher = [[self alloc] init];
    fetcher.client = [LSClient client];
    
    fetcher.connectionInfo = [LSConnectionInfo connectionInfoWithPushServerURL:trubuzzPushServerURL pushServerControlURL:nil user:nil password:nil adapter:trubuzzConnectionAdapterName];
    
    fetcher.fetchQueue = dispatch_queue_create("com.trubuzzChallenges.fetchQueue", dispatch_queue_attr_make_with_qos_class(DISPATCH_QUEUE_SERIAL, QOS_CLASS_BACKGROUND, 0));
    
    [LSLog enableSourceType:LOG_SRC_CLIENT];
    
    return fetcher;
}

- (void)openConnectionWithCompletionHandler:(void (^)(bool))completionHandler {
    
    dispatch_async(self.fetchQueue, ^{
        
        NSError* connectionError = nil;
        _connectionHandler = completionHandler;
        [self.client openConnectionWithInfo:self.connectionInfo delegate:self error:&connectionError];
        
        if (connectionError){
            if (completionHandler)
                completionHandler(false);
        }
        
    });

}

- (void)closeConnection {
    
    dispatch_async(self.fetchQueue, ^{
    
        [self.client closeConnection];
    
    });
    
}

- (void)subscribeToDemoTrubuzzSymbolsWithDelegate:(id<LSTableDelegate>)delegate {
    
    dispatch_async(self.fetchQueue, ^{
    
        LSExtendedTableInfo* extendedTableInfo = [LSExtendedTableInfo extendedTableInfoWithItems:trubuzzTableItems mode:LSModeMerge fields:trubuzzTableFields dataAdapter:trubuzzDataAdapter snapshot:YES];
        NSError* subscriptionError = nil;
        
        
        self.subscriptionKey = [self.client subscribeTableWithExtendedInfo:extendedTableInfo delegate:delegate useCommandLogic:NO error:&subscriptionError];
        
        if (subscriptionError) {
            NSLog(@"couldn't subscribe");
        } else {
            NSLog(@"subscribed succesfully");
        }
    
    });
}

- (NSArray*)symbols { return trubuzzTableItems; }

#pragma mark - LSConnectionDelegate

- (void)clientConnection:(LSClient *)client didStartSessionWithPolling:(BOOL)polling {
    
    if (_connectionHandler)
        _connectionHandler(true);
}

- (void) clientConnection:(LSClient *)client didChangeActivityWarningStatus:(BOOL)warningStatus {
    NSLog(@"Connector: activity warning status changed: %@", (warningStatus ? @"ON" : @"OFF"));
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONN_STATUS object:self];
}

- (void) clientConnectionDidEstablish:(LSClient *)client {
    NSLog(@"Connector: connection established");
}

- (void) clientConnectionDidClose:(LSClient *)client {
    NSLog(@"Connector: connection closed");
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONN_STATUS object:self];
    
    // This event is called just by manually closing the connection,
    // never happens in this example.
}

- (void) clientConnection:(LSClient *)client didEndWithCause:(int)cause {
    NSLog(@"Connector: connection ended with cause: %d", cause);
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONN_STATUS object:self];
    
    // In this case the session has been forcibly closed by the server,
    // the LSClient will not automatically reconnect, notify the observers
    //[[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONN_ENDED object:self];
}

- (void) clientConnection:(LSClient *)client didReceiveDataError:(LSPushServerException *)error {
    NSLog(@"Connector: data error: %@", error);
}

- (void) clientConnection:(LSClient *)client didReceiveServerFailure:(LSPushServerException *)failure {
    NSLog(@"Connector: server failure: %@", failure);
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONN_STATUS object:self];
    
    // The LSClient will reconnect automatically in this case.
}

- (void) clientConnection:(LSClient *)client didReceiveConnectionFailure:(LSPushConnectionException *)failure {
    NSLog(@"Connector: connection failure: %@", failure);
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONN_STATUS object:self];
    
    // The LSClient will reconnect automatically in this case.
}


@end
