//
//  TBTableViewController.m
//  TrubuzzChallenges
//
//  Created by Taras Vozniuk on 5/7/15.
//  Copyright (c) 2015 ambientlight. All rights reserved.
//

#import "TBTableViewController.h"
#import "TBHistoricFetcher.h"
#import "TBRealtimeFetcher.h"
#import "TBStockChartViewController.h"
#import "TBRealtimeQuoteTableViewController.h"
#import "MBProgressHUD.h"

@implementation TBTableViewController

- (void)viewDidLoad {
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
    self.navigationItem.titleView = imageView;
    
    
}

#pragma mark - UITableViewDelegate && UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:@"tbCell" forIndexPath:indexPath];
    cell.textLabel.text = [NSString stringWithFormat:@"Challenge %ld", indexPath.row+1];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *realtimeVC = [sb instantiateViewControllerWithIdentifier:@"realtimeVC"];
        
        
        [self.navigationController pushViewController:realtimeVC animated:YES];
        
    } else if (indexPath.row == 1){
        
        
        MBProgressHUD* progressIndicator = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        progressIndicator.labelText = @"fetching data...";
        
        
        [TBHistoricFetcher fetchQuotesForCountry:@"US" symbol:@"AAPL" exchange:@"XNAS" barCount:128 period:TBHistoricQuotePeriodDaily andCompletionHandler:^(NSArray* historicQuotes){
        
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [progressIndicator hide:YES];
                
                if (historicQuotes) {
                    
                    TBStockChartViewController* chartVC = [[TBStockChartViewController alloc] initWithQuotes:historicQuotes];
                    [self.navigationController pushViewController:chartVC animated:YES];
                    
                    
                }
            
            });
        
        }];
       
        
    }
}

@end
