//
//  TBCandlestickSeries.m
//  TrubuzzChallenges
//
//  Created by Taras Vozniuk on 5/8/15.
//  Copyright (c) 2015 ambientlight. All rights reserved.
//

#import "TBCandlestickSeries.h"
#import "TBHistoricQuote.h"

@implementation TBCandlestickSeries

+ (instancetype)defaultSeries {
    
    TBCandlestickSeries* series = [[TBCandlestickSeries alloc] init];
    return series;
}

- (SChartCandlestickSeriesStyle*)styleForPoint:(id<SChartData>)point previousPoint:(id<SChartData>)prevPoint {
 
    SChartCandlestickSeriesStyle *seriesStyle = [super styleForPoint:point previousPoint:prevPoint];
    seriesStyle.outlineWidth = [NSNumber numberWithFloat:1.0];
    seriesStyle.stickWidth = [NSNumber numberWithFloat:1.0];
    seriesStyle.stickColor = UIColor.grayColor;
    
    float open = [[point sChartYValueForKey: SChartCandlestickKeyOpen] floatValue];
    float close = [[point sChartYValueForKey: SChartCandlestickKeyClose] floatValue];
    float previousClose = [[prevPoint sChartYValueForKey: SChartCandlestickKeyClose] floatValue];

    UIColor* candlestickColor = (close > previousClose) ? UIColor.greenColor : UIColor.redColor;
    seriesStyle.outlineColor = candlestickColor;
    
    
    if (close > open) {
        seriesStyle.risingColor = candlestickColor;
        seriesStyle.risingColorGradient = candlestickColor;
    } else {
        seriesStyle.fallingColor = candlestickColor;
        seriesStyle.fallingColorGradient = candlestickColor;
    }
    
    return seriesStyle;
}

@end
