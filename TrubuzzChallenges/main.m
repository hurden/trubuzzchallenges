//
//  main.m
//  TrubuzzChallenges
//
//  Created by Taras Vozniuk on 5/6/15.
//  Copyright (c) 2015 ambientlight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
