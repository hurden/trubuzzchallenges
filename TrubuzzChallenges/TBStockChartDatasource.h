//
//  TBStockChartDatasource.h
//  TrubuzzChallenges
//
//  Created by Taras Vozniuk on 5/8/15.
//  Copyright (c) 2015 ambientlight. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ShinobiCharts/ShinobiCharts.h>

@interface TBStockChartDatasource : NSObject <SChartDatasource>

+ (instancetype)datasourceWithHistoricQuotes:(NSArray*)quotes;

@end
