//
//  TBStockChartViewController.m
//  TrubuzzChallenges
//
//  Created by Taras Vozniuk on 5/8/15.
//  Copyright (c) 2015 ambientlight. All rights reserved.
//

#import "TBStockChartViewController.h"
#import "TBCandlestickSeries.h"

#define licenceKey @"ZaBec2+LshymEJSMjAxNTA2MDZwcmltYXJ5LnRhcmFzLnZvem5pdWtAZ21haWwuY29tJZZtFMANyH+fKi4fwVqMo3eo75lrisxCyUf9Ap+ZPe4Iz2Wgr+faPpsAd9sKMy8mcxl6053gWrOeh4Ma2g6NDT1M1yrP9YH5NFgGJKeCburJyvxyzoXq7gFj7tHdHQS0MXLeIrCackCOOW1fd9OsCNUUiMr8=BQxSUisl3BaWf/7myRmmlIjRnMU2cA7q+/03ZX9wdj30RzapYANf51ee3Pi8m2rVW6aD7t6Hi4Qy5vv9xpaQYXF5T7XzsafhzS3hbBokp36BoJZg8IrceBj742nQajYyV7trx5GIw9jy/V6r0bvctKYwTim7Kzq+YPWGMtqtQoU=PFJTQUtleVZhbHVlPjxNb2R1bHVzPnh6YlRrc2dYWWJvQUh5VGR6dkNzQXUrUVAxQnM5b2VrZUxxZVdacnRFbUx3OHZlWStBK3pteXg4NGpJbFkzT2hGdlNYbHZDSjlKVGZQTTF4S2ZweWZBVXBGeXgxRnVBMThOcDNETUxXR1JJbTJ6WXA3a1YyMEdYZGU3RnJyTHZjdGhIbW1BZ21PTTdwMFBsNWlSKzNVMDg5M1N4b2hCZlJ5RHdEeE9vdDNlMD08L01vZHVsdXM+PEV4cG9uZW50PkFRQUI8L0V4cG9uZW50PjwvUlNBS2V5VmFsdWU+"

#import "TBHistoricQuote.h"
#import "TBVolumeDataSource.h"
#import "TBMultiValueTooltip.h"

#define volumeViewDefaultHight 100

@interface TBStockChartViewController()

@property (nonatomic, strong) ShinobiChart* chart;
@property (nonatomic, strong) ShinobiChart* volumeChart;

@property (nonatomic, strong) NSMutableArray* OHLCSeries;
@property (nonatomic, strong) TBVolumeDataSource* volumeDataSource;


@property (nonatomic, strong) NSArray* quotes;


@property (nonatomic, assign) NSUInteger eOffset;

- (SChartTheme*)defaultTheme;

@end

@implementation TBStockChartViewController {
    NSUInteger _refreshCount;
    NSTimer* _refreshTimer;
}

- (instancetype)initWithQuotes:(NSArray*)quotes {
    
    self = [super init];
    if (self){
        _quotes = quotes;
        _volumeDataSource = [[TBVolumeDataSource alloc] initWithQuotes:quotes];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
    self.navigationItem.titleView = imageView;
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    [self setupDataSeries];
    
    
    CGFloat volumeViewHeight = volumeViewDefaultHight;
    
    CGFloat margin = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) ? 60.0 : 90.0;
    
    self.chart = [self setupShinobiChartWithFrame:CGRectMake(0, margin, self.view.bounds.size.width - 10, self.view.bounds.size.height - margin - volumeViewHeight - 10) datasource:self];
    self.volumeDataSource.mainChart = self.chart;
    self.volumeChart = [self setupShinobiChartWithFrame:CGRectMake(0, margin + self.chart.frame.size.height, self.view.bounds.size.width - 10, volumeViewDefaultHight) datasource:self.volumeDataSource];
    self.volumeChart.xAxis.style.majorTickStyle.showLabels = true;
    
    ((SChartCrosshair*)self.chart.crosshair).tooltip = [[TBMultiValueTooltip alloc] init];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.chart.crosshair hide];
}

- (ShinobiChart*)setupShinobiChartWithFrame:(CGRect)frame datasource:(id<SChartDatasource, SChartDelegate>)datasourceAndDelegate
{
    ShinobiChart* chart = [[ShinobiChart alloc] initWithFrame:frame];
    [chart applyTheme:self.defaultTheme];
    
    chart.autoresizingMask =  ~UIViewAutoresizingNone;
    chart.licenseKey = licenceKey;
    
    
    SChartDiscontinuousDateTimeAxis *xAxis = [[SChartDiscontinuousDateTimeAxis alloc] init];
    [self excludeDiscontiniousPeriodsFromAxis: xAxis];
    chart.xAxis = xAxis;
    
    
    
    //show only last 50 elements
    NSDate* lastQouteDate = ((TBHistoricQuote*)self.quotes.lastObject).date;
    if (self.quotes.count > 50){
        
        NSDate* fiftyElementsAgo = ((TBHistoricQuote*)self.quotes[self.quotes.count - 51]).date;
        
        NSDate* endElementAdjustedToOffset = [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitDay
                                                                                      value:-self.eOffset
                                                                                     toDate:lastQouteDate
                                                                                    options:0];
        NSDate* startElementAdjustedToOffset = [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitDay
                                                                                        value:-self.eOffset
                                                                                       toDate:fiftyElementsAgo
                                                                                      options:0];
        
        chart.xAxis.defaultRange = [[SChartDateRange alloc] initWithDateMinimum:startElementAdjustedToOffset andDateMaximum:endElementAdjustedToOffset];
    }
    
    self.eOffset = 0;
    
    

    
    SChartNumberAxis *yAxis = [[SChartNumberAxis alloc] init];
    yAxis.axisPosition = SChartAxisPositionReverse;
    
    chart.yAxis = yAxis;
    
    
    yAxis.enableGesturePanning = YES;
    yAxis.enableGestureZooming = YES;
    xAxis.enableGesturePanning = YES;
    xAxis.enableGestureZooming = YES;
    
    
    [self.view addSubview:chart];
    
    chart.datasource = datasourceAndDelegate;
    chart.delegate = datasourceAndDelegate;
    return chart;
}

- (void)excludeDiscontiniousPeriodsFromAxis:(SChartDiscontinuousDateTimeAxis*)xAxis {
    
    for (NSUInteger iter = 0; iter < self.quotes.count - 1; iter++) {
        
        NSDate* thisDate = ((TBHistoricQuote*)self.quotes[iter]).date;
        NSDate* nextDate = [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitDay
                                                                    value:1
                                                                   toDate:thisDate
                                                                  options:0];
        NSDate* nextSeriesElementDate = ((TBHistoricQuote*)self.quotes[iter+1]).date;
        
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        
        //NSLog(@"%@ ::: %@", nextDate, nextSeriesElementDate);
        
        
        if ([nextDate compare:nextSeriesElementDate] == NSOrderedAscending ) {
            
            NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                                                       fromDate:nextDate
                                                         toDate:nextSeriesElementDate
                                                        options:0];
            
            [xAxis addExcludedTimePeriod:[[SChartTimePeriod alloc] initWithStart:nextDate andLength:[[SChartDateFrequency alloc] initWithDay:components.day]]];
            self.eOffset += components.day;
        }
    }
}

- (SChartTheme*)defaultTheme {
    
    SChartTheme* theme = [[SChartiOS7Theme alloc] init];
    theme.chartStyle.backgroundColor = UIColor.whiteColor;
    
    theme.chartTitleStyle.font = [UIFont systemFontOfSize:20];
    theme.chartTitleStyle.textColor = UIColor.blackColor;
    theme.chartTitleStyle.titleCentresOn = SChartTitleCentresOnChart;
    theme.chartTitleStyle.overlapChartTitle = true;
    
    theme.xAxisStyle.lineColor = UIColor.grayColor;
    theme.xAxisStyle.titleStyle.font = [UIFont systemFontOfSize:16];
    theme.xAxisStyle.majorTickStyle.lineColor = UIColor.blackColor;
    theme.xAxisStyle.majorTickStyle.labelColor = UIColor.blackColor;
    theme.xAxisStyle.majorTickStyle.labelFont = [UIFont systemFontOfSize:14];
    theme.xAxisStyle.majorTickStyle.showTicks = true;
    theme.xAxisStyle.majorTickStyle.showLabels = false;
    theme.xAxisStyle.majorTickStyle.tickGap = @0;
    theme.xAxisStyle.majorGridLineStyle.showMajorGridLines = true;
    theme.xAxisStyle.majorGridLineStyle.lineWidth = @1;
    theme.xAxisStyle.majorGridLineStyle.lineColor = UIColor.lightGrayColor;
    
    theme.yAxisStyle.lineColor = theme.xAxisStyle.lineColor;
    theme.yAxisStyle.majorTickStyle.lineColor = UIColor.blackColor;
    theme.yAxisStyle.majorTickStyle.labelColor = UIColor.blackColor;
    theme.yAxisStyle.majorTickStyle.labelFont = [UIFont systemFontOfSize:14];
    theme.yAxisStyle.majorTickStyle.lineLength = @8;
    theme.yAxisStyle.majorTickStyle.lineWidth = @1;
    theme.yAxisStyle.majorTickStyle.showTicks = false;
    theme.yAxisStyle.majorTickStyle.showLabels = true;
    theme.yAxisStyle.majorGridLineStyle.showMajorGridLines = true;
    theme.yAxisStyle.majorGridLineStyle.lineWidth = @1;
    theme.yAxisStyle.majorGridLineStyle.lineColor = UIColor.lightGrayColor;
    
    return theme;
}


- (void)setupDataSeries {
    
    self.OHLCSeries = [NSMutableArray array];
    //candlesticks
    for (TBHistoricQuote* quote in self.quotes) {
        
        SChartMultiYDataPoint* datapoint = [[SChartMultiYDataPoint alloc] init];
        datapoint.xValue = quote.date;
        
        NSDictionary* yValues = @{SChartCandlestickKeyOpen: @(quote.open),
                                  SChartCandlestickKeyHigh: @(quote.high),
                                  SChartCandlestickKeyLow: @(quote.low),
                                  SChartCandlestickKeyClose: @(quote.close)};
        
        datapoint.yValues = [NSMutableDictionary dictionaryWithDictionary:yValues];
        [self.OHLCSeries addObject:datapoint];
    }
}


#pragma mark - SChartDatasource

- (NSInteger)numberOfSeriesInSChart:(ShinobiChart *)chart {
    return 1;
}

-(SChartSeries *)sChart:(ShinobiChart *)chart seriesAtIndex:(NSInteger)index {
    
    TBCandlestickSeries* candlestickSeries = [TBCandlestickSeries defaultSeries];
    candlestickSeries.crosshairEnabled = true;
    return candlestickSeries;
}

- (NSInteger)sChart:(ShinobiChart *)chart numberOfDataPointsForSeriesAtIndex:(NSInteger)seriesIndex {
    return self.OHLCSeries.count;
}

- (id<SChartData>)sChart:(ShinobiChart *)chart dataPointAtIndex:(NSInteger)dataIndex forSeriesAtIndex:(NSInteger)seriesIndex {
    return self.OHLCSeries[dataIndex];
}

#pragma mark - SChartDelegate

- (void)sChartIsPanning:(ShinobiChart *)chart {

    [self syncronizeVolumeChartFromChart:self.chart];
}

- (void)sChartDidFinishPanning:(ShinobiChart *)chart
{
    //NSLog(@"didFinishPanning stockChart");
    _refreshCount = 50;
    
    // workaround to syncronize charts after panning is finished but chart still moves, since panning was done
    // on the border of the data series
    // refresh on 50Frames per sec (0.02 of a sec)
    
    // still don't forget to add condition to check for a dataRange end, don't resynch in the middle of dataRange
    
    _refreshTimer = [NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:@selector(handleRefresh) userInfo:nil repeats:YES];
}

- (void)handleRefresh
{
    [self syncronizeVolumeChartFromChart:self.chart];
    _refreshCount--;
    
    if (_refreshCount == 0) {
        [_refreshTimer invalidate];
    }
}

- (void)sChartIsZooming:(ShinobiChart *)chart {
    
    [self syncronizeVolumeChartFromChart:self.chart];
}

// This method updates all charts in the _managedCharts array with the chart's axis ranges passed in as the parameter.

- (void)syncronizeVolumeChartFromChart:(ShinobiChart* )chart {
    
    SChartRange *zoomedXRange = chart.xAxis.axisRange;
    
    [self.volumeChart.xAxis setRangeWithMinimum:zoomedXRange.minimum andMaximum:zoomedXRange.maximum];
    [self.volumeChart redrawChart];
}



@end
