//
//  TBHistoricQuote.h
//  TrubuzzChallenges
//
//  Created by Taras Vozniuk on 5/7/15.
//  Copyright (c) 2015 ambientlight. All rights reserved.
//

#import <Foundation/Foundation.h>

#define entryTimeKey @"mdEntryTime"
#define openKey @"firstPx"
#define closeKey @"price"
#define highKey @"highPx"
#define lowKey @"lowPx"
#define volumeKey @"totalVolumeTraded"

@interface TBHistoricQuote : NSObject

+ (instancetype)quoteFromDictionary:(NSDictionary*)quoteDict;

@property (nonatomic, strong, readonly) NSDate* date;
@property (nonatomic, assign, readonly) double open;
@property (nonatomic, assign, readonly) double close;
@property (nonatomic, assign, readonly) double high;
@property (nonatomic, assign, readonly) double low;
@property (nonatomic, assign, readonly) unsigned long long volume;

@end
