//
//  TBStockChartViewController.m
//  TrubuzzChallenges
//
//  Created by Taras Vozniuk on 5/8/15.
//  Copyright (c) 2015 ambientlight. All rights reserved.
//

#import "TBStockChartViewController.h"
#import "TBStockChartDatasource.h"

#define licenceKey @"ZaBec2+LshymEJSMjAxNTA2MDZwcmltYXJ5LnRhcmFzLnZvem5pdWtAZ21haWwuY29tJZZtFMANyH+fKi4fwVqMo3eo75lrisxCyUf9Ap+ZPe4Iz2Wgr+faPpsAd9sKMy8mcxl6053gWrOeh4Ma2g6NDT1M1yrP9YH5NFgGJKeCburJyvxyzoXq7gFj7tHdHQS0MXLeIrCackCOOW1fd9OsCNUUiMr8=BQxSUisl3BaWf/7myRmmlIjRnMU2cA7q+/03ZX9wdj30RzapYANf51ee3Pi8m2rVW6aD7t6Hi4Qy5vv9xpaQYXF5T7XzsafhzS3hbBokp36BoJZg8IrceBj742nQajYyV7trx5GIw9jy/V6r0bvctKYwTim7Kzq+YPWGMtqtQoU=PFJTQUtleVZhbHVlPjxNb2R1bHVzPnh6YlRrc2dYWWJvQUh5VGR6dkNzQXUrUVAxQnM5b2VrZUxxZVdacnRFbUx3OHZlWStBK3pteXg4NGpJbFkzT2hGdlNYbHZDSjlKVGZQTTF4S2ZweWZBVXBGeXgxRnVBMThOcDNETUxXR1JJbTJ6WXA3a1YyMEdYZGU3RnJyTHZjdGhIbW1BZ21PTTdwMFBsNWlSKzNVMDg5M1N4b2hCZlJ5RHdEeE9vdDNlMD08L01vZHVsdXM+PEV4cG9uZW50PkFRQUI8L0V4cG9uZW50PjwvUlNBS2V5VmFsdWU+"

@interface TBStockChartViewController()

@property (nonatomic, strong) TBStockChartDatasource* datasource;
@property (nonatomic, strong) ShinobiChart* candlestickChart;

- (SChartTheme*)defaultTheme;

@end


@implementation TBStockChartViewController


- (instancetype)initWithHistoricQuotes:(NSArray*)historicQuotes {
    
    self = [super init];
    if (self){
        
        _datasource = [TBStockChartDatasource datasourceWithHistoricQuotes:historicQuotes];
        
    }
    
    return self;
}


- (void)viewDidLoad {
    
    [ShinobiCharts setLicenseKey:licenceKey];
    
    self.candlestickChart = [[ShinobiChart alloc] initWithFrame:self.view.bounds];
    self.candlestickChart.datasource = self.datasource;
    
    self.candlestickChart.autoresizingMask = ~UIViewAutoresizingNone;
    self.candlestickChart.rotatesOnDeviceRotation = NO;
    
    SChartDateTimeAxis *xAxis = [SChartDateTimeAxis new];
    xAxis.enableGesturePanning = NO;
    xAxis.enableGestureZooming = NO;
    xAxis.enableMomentumPanning = NO;
    xAxis.enableMomentumZooming = NO;
    
    
    // Create a number axis to use as the y axis.
    SChartNumberAxis *yAxis = [SChartNumberAxis new];
    
    // Enable panning and zooming on Y
    yAxis.enableGesturePanning = YES;
    yAxis.enableGestureZooming = YES;
    yAxis.enableMomentumPanning = YES;
    yAxis.enableMomentumZooming = YES;
    yAxis.axisPosition = SChartAxisPositionReverse;
    yAxis.width = @50;
    
    self.candlestickChart.xAxis = xAxis;
    self.candlestickChart.yAxis = yAxis;
    
    self.candlestickChart.loadDataInBackground = NO;
    
    
    
    
    // Set the initial start and end values for the x axis on the main chart
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [NSDateComponents new];
    components.day = 1;
    components.month = 1;
    components.year = 2014;
    
    NSDate *startX = [calendar dateFromComponents:components];
    
    components.year = 2015;
    NSDate *endX = [calendar dateFromComponents:components];
    
    // We have to set the default x-axis range
    self.candlestickChart.xAxis.defaultRange = [[SChartDateRange alloc] initWithDateMinimum:startX
                                                                      andDateMaximum:endX];
    
    
    
    
    [self.candlestickChart applyTheme:self.defaultTheme];
    self.candlestickChart.clipsToBounds = NO;
    
    //self.candlestickChart.gestureDoubleTapResetsZoom = YES;
    //self.candlestickChart.gestureDoubleTapEnabled = YES;
    
    
    SChartNumberRange *numberRange = [[SChartNumberRange alloc] initWithMinimum:@100
                                                                     andMaximum:@150];
    self.candlestickChart.yAxis.defaultRange = numberRange;

    
    
    //self.candlestickChart.delegate = self;
    [self.view addSubview:self.candlestickChart];
}

- (SChartTheme*)defaultTheme {
    
    SChartTheme* theme = [[SChartiOS7Theme alloc] init];
    theme.chartStyle.backgroundColor = UIColor.whiteColor;
    
    theme.chartTitleStyle.font = [UIFont systemFontOfSize:20];
    theme.chartTitleStyle.textColor = UIColor.blackColor;
    theme.chartTitleStyle.titleCentresOn = SChartTitleCentresOnChart;
    theme.chartTitleStyle.overlapChartTitle = false;

    theme.xAxisStyle.lineColor = UIColor.grayColor;
    theme.xAxisStyle.titleStyle.font = [UIFont systemFontOfSize:16];
    theme.xAxisStyle.majorTickStyle.lineColor = UIColor.blackColor;
    theme.xAxisStyle.majorTickStyle.labelColor = UIColor.whiteColor;
    theme.xAxisStyle.majorTickStyle.labelFont = [UIFont systemFontOfSize:14];
    theme.xAxisStyle.majorTickStyle.showTicks = false;
    theme.xAxisStyle.majorTickStyle.showLabels = false;
    theme.xAxisStyle.majorTickStyle.tickGap = @0;
    
    theme.yAxisStyle.lineColor = theme.xAxisStyle.lineColor;
    theme.yAxisStyle.majorTickStyle.lineColor = UIColor.blackColor;
    theme.yAxisStyle.majorTickStyle.labelColor = UIColor.whiteColor;
    theme.yAxisStyle.majorTickStyle.labelFont = [UIFont systemFontOfSize:14];
    theme.yAxisStyle.majorTickStyle.lineLength = @8;
    theme.yAxisStyle.majorTickStyle.lineWidth = @1;
    theme.yAxisStyle.majorTickStyle.showTicks = NO;
    theme.yAxisStyle.majorTickStyle.showLabels = NO;

    return theme;
}



@end
