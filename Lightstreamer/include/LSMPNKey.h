//
//  LSMPNKey.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>


/**
 * The LSMPNKey class contains the subscription ID to be used to access an MPN subscription.
 */
@interface LSMPNKey : NSObject <NSCopying> {}


#pragma mark -
#pragma mark Initialization

/**
 * Creates and returns an LSMPNKey object with the specified parameters.
 *
 * @param subscriptionId The subscription ID of the MPN subscription.
 *
 * @return The LSMPNKey object.
 */
+ (LSMPNKey *) mpnKeyWithSubscriptionId:(NSString *)subscriptionId;

/**
 * Initializes an LSMPNKey object with the specified parameters.
 *
 * @param subscriptionId The subscription ID of the MPN subscription.
 *
 * @return The LSMPNKey object.
 */
- (id) initWithSubscriptionId:(NSString *)subscriptionId;


#pragma mark -
#pragma mark Properties

/**
 * The subscription ID to be used to access the MPN subscription.
 */
@property (nonatomic, readonly) NSString *subscriptionId;


@end
