//
//  LSMPNSubscription.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>


@class LSClient;
@class LSMPNInfo;
@class LSMPNKey;
@class LSMPNStatusInfo;


/**
 * The LSMPNSubscription class wraps the key (as an LSMPNKey object) and the specifications (as an LSMPNInfo object) of
 * a mobile push notifications (MPN) subscription. It represents a view on the actual MPN subscription stored on the Server's
 * database. The LSMPNSubscription class supports operations like checking its current status, modifying the specifications and 
 * deactivating the MPN subscription.
 * <BR/>MPN subscriptions are cached locally and the cache is stored in the LSClient instance that originated (i.e. activated or inquired) the MPN subscription.
 * Thus, once an LSMPNSubscription object, with a certain MPN key, has been obtained from an LSCLient instance, any operation returning 
 * the same MPN subscription from the same LSClient instance will return the same LSMPNSubscription instance. On the other hand, 
 * deactivating an MPN subscription removes it from the local cache and marks its corresponding LSMPNSubscription instance as #deactivated.
 * <BR/>Moreover, the LSClient local cache survives a reconnection: if the LSClient reconnects (automatically or manually) with the same base parameters
 * (i.e. LSConnectionInfo#pushServerURL, LSConnectionInfo#adapter and LSConnectionInfo#user), the MPN subscription cache is preserved. If one of 
 * these parameters changes, the cache is cleared and any dangling LSMPNSubscription instance is made invalid (no more operations will be possible on it).
 * <BR/>LSMPNSubscription instances keep a reference to their originating #LSClient, see #client. In order for any operation to be successful, the
 * originating #LSClient must be connected at the moment the operation is executed.
 * <BR/>Note that, while the LSMPNkey instance is constant, the LSMPNInfo instance may be replaced by calls that modify the MPN subscription's specifications,
 * e.g. #modify:, or LSClient#activateMPN:coalescing: with <CODE>coalescing</CODE> flag set, or by calls that update the local cache from the Server, 
 * e.g. LSClient#inquireMPN:, LSClient#inquireMPNsWithStatus: and LSClient#inquireAllMPNs.
 * <BR/>Since MPN subscriptions are stored on the Server on a per-device and per-app basis, under normal conditions only the device/app pair that 
 * created an MPN subscription may be accessing or modifying it or its cached LSMPNSubscription representation.
 */
@interface LSMPNSubscription : NSObject


#pragma mark -
#pragma mark Initialization

/**
 * Creates and returns an LSMPNSubscription object with the specified parameters.
 * <BR/>This factory method is for internal use only.
 *
 * @param mpnKey Key of the MPN subscription (see LSMPNKey for more information).
 * @param mpnInfo Specifications of the MPN subscription. An unmodifiable copy of the object is stored internally.
 */
+ (LSMPNSubscription *) mpnSubscriptionWithKey:(LSMPNKey *)mpnKey info:(LSMPNInfo *)mpnInfo;

/**
 * Initializes an LSMPNSubscription object with the specified parameters.
 * <BR/>This initializer is for internal use only.
 *
 * @param mpnKey Key of the MPN subscription (see LSMPNKey for more information).
 * @param mpnInfo Specifications of the MPN subscription. An unmodifiable copy of the object is stored internally.
 */
- (id) initWithKey:(LSMPNKey *)mpnKey info:(LSMPNInfo *)mpnInfo;


#pragma mark -
#pragma mark Operations

/**
 * Inquires the status of the MPN subscription on the Server.
 * <BR/>If the MPN subscription has been forcibly deactivated on the Server, the call will result in a LSPushServerException
 * with error code 46 (subscription unknown) or 45 (device unknown, if no more subscriptions exist for this device and application).
 * <BR/>Note that the status of an MPN subscription is never cached.
 * <BR/>The method is blocking if executed outside of a batch, otherwise it is non-blocking: outside of a batch, it will return
 * only after receiving the Server response. Inside a batch, the method will return immediately with nil,
 * but the request will be processed only on batch commit (see LSClient#commitBatch).
 *
 * @return The MPN subscription status info, i.e. whether it is active or triggered,
 * and the time when the status has been set. Contains also status info about the MPN device, i.e.
 * whether it is active or suspended and the corresponding time.
 *
 * @throws LSPushClientException Thrown in case the MPN subscription has been deactivated,
 * or in case the context is invalid (e.g. can't obtain a valid device token or app ID).
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (LSMPNStatusInfo *) checkStatus;

/**
 * Inquires the status of the MPN subscription on the Server.
 * Non-exception-throwing variant of #checkStatus, see for more information.
 *
 * @param error In case of error, upon return contains an NSError object that describes the problem.
 *
 * @return The MPN subscription status info, i.e. whether it is active or triggered,
 * and the time when the status has been set. Contains also status info about the MPN device, i.e.
 * whether it is active or suspended and the corresponding time.
 *
 * @throws LSPushClientException Thrown in case the MPN subscription has been deactivated,
 * or in case the context is invalid (e.g. can't obtain a valid device token or app ID).
 */
- (LSMPNStatusInfo *) checkStatus:(NSError * __autoreleasing *)error;

/**
 * Modifies the specifications of the MPN subscription. Once the change has been submitted to the server, the #mpnInfo
 * property is updated with the passed mpnInfo instance, whereas the MPN key remains unchanged.
 * <BR/>If the MPN subscription has been forcibly deactivated on the Server, the call will result in a LSPushServerException
 * with error code 46 (subscription unknown) or 45 (device unknown, if no more subscriptions exist for this device and application).
 * <BR/>The operation works only on the current MPN subscription (i.e. the MPN subscription identified by the MPN key). No
 * coalescence is possible with this operation: even if the new specifications had the same adapter set, data adapter, group, schema
 * and trigger expression of another MPN subscription, this MPN subscription would remain distinct. See 
 * LSClient#activateMPN:coalescing: for more information on coalescence.
 * <BR/>The same limitations that apply during activation (see LSClient#activateMPN:coalescing:) apply during modification. Additionally,
 * moving MPN subscriptions from an adapter set to another is not allowed. This means the #LSTableInfo#adapterSet field
 * must be the same of the original MPN subscription AND the same of the current connection. If this field is nil, the
 * adapter set of the current connection is considered as specified.
 * <BR/>Note that MPN subscriptions are cached locally, see LSMPNSubscription description for more information.
 * This method always contacts the Server and updates the related MPN subscription in the local cache.
 * <BR/>The method is blocking if executed outside of a batch, otherwise it is non-blocking: outside of a batch, it will return
 * only after receiving the Server response. Inside a batch, the method will return immediately, but the request will be processed 
 * only on batch commit (see LSClient#commitBatch).
 *
 * @param mpnInfo Contains the new table and format specifications of the mobile push notifications. An unmodifiable copy of the object is stored internally.
 *
 * @throws LSPushClientException Thrown in case the MPN subscription has been deactivated,
 * or in case the context is invalid (e.g. can't obtain a valid device token or app ID).
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (void) modify:(LSMPNInfo *)mpnInfo;

/**
 * Modifies the specifications of the MPN subscription.
 * Non-exception-throwing variant of #modify:, see for more information.
 *
 * @param mpnInfo Contains the new table and format specifications of the mobile push notifications. An unmodifiable copy of the object is stored internally.
 * @param error In case of error, upon return contains an NSError object that describes the problem.
 *
 * @return YES if the MPN subscription could be modified, NO if it couldn't. In the latter case, an error is provided.
 *
 * @throws LSPushClientException Thrown in case the MPN subscription has been deactivated,
 * or in case the context is invalid (e.g. can't obtain a valid device token or app ID).
 */
- (BOOL) modify:(LSMPNInfo *)mpnInfo error:(NSError * __autoreleasing *)error;

/**
 * Deactivates the MPN subscription. Once deactivated, no more mobile push (i.e. remote) notifications will be sent on table update. 
 * A deactivation has also the effect of deleting the MPN subscription.
 * <BR/>If the MPN subscription has been forcibly deactivated on the Server, the call will result in a LSPushServerException
 * with error code 46 (subscription unknown) or 45 (device unknown, if no more subscriptions exist for this device and application).
 * <BR/>Note that MPN subscriptions are cached locally, see LSMPNSubscription description for more information.  
 * This method always contacts the Server and updates the related MPN subscription in the local cache.
 * <BR/>The method is blocking if executed outside of a batch, else it is non-blocking: outside of a batch, it will return
 * only after receiving the Server response. Inside a batch, the method will return immediately but the request will be processed
 * only on batch commit (see LSClient#commitBatch).
 *
 * @throws LSPushClientException Thrown in case the MPN subscription has already been deactivated,
 * or in case the context is invalid (e.g. can't obtain a valid device token or app ID).
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (void) deactivate;

/**
 * Deactivates the MPN subscription.
 * Non-exception-throwing variant of #deactivate, see for more information.
 *
 * @param error In case of error, upon return contains an NSError object that describes the problem.
 *
 * @return YES if the MPN subscription could be deactivated, NO if it couldn't. In the latter case, an error is provided.
 *
 * @throws LSPushClientException Thrown in case the MPN subscription has already been deactivated,
 * or in case the context is invalid (e.g. can't obtain a valid device token or app ID).
 */
- (BOOL) deactivate:(NSError * __autoreleasing *)error;


#pragma mark -
#pragma mark Properties

/**
 * The LSClient instance that originated (i.e. activated or inquired) this LSMPNSubscription instance.
 */
@property (nonatomic, weak, readonly) LSClient *client;

/**
 * The MPN key identifying this MPN subscription.
 */
@property (nonatomic, readonly) LSMPNKey *mpnKey;

/**
 * The specifications of this MPN subscription. The returned object is unmodifiable:
 * trying to change one of its properties causes an LSPushClientException to be raised.
 * <BR/>The stored object may be replaced over time by calls that modify the MPN subscription's specifications,
 * e.g. #modify:, or LSClient#activateMPN:coalescing: with <CODE>coalescing</CODE> flag set, or by
 * calls that update the local cache from the Server, e.g. LSClient#inquireMPN:, LSClient#inquireMPNsWithStatus: and LSClient#inquireAllMPNs.
 * <BR/>See the LSMPNSubscription description for more information.
 */
@property (nonatomic, readonly) LSMPNInfo *mpnInfo;

/**
 * Tells if this MPN subscription has been deactivated or not. May be changed by deactivation calls,
 * e.g. #deactivate, or LSClient#deactivateMPNsWithStatus: and LSClient#deactivateAllMPNs, or by
 * calls that update the local cache from the Server, e.g. LSClient#inquireMPN: and LSClient#inquireAllMPNs.
 * <BR/>See the LSMPNSubscription description for more information.
 */
@property (nonatomic, readonly) BOOL deactivated;


@end
