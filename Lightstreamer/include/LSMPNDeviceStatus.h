//
//  LSMPNDeviceStatus.h
//  Lightstreamer client for iOS
//
//  Created by Gianluca Bertani on 01/10/14.
//  Copyright (c) 2014 Weswit srl. All rights reserved.
//

#ifndef Lightstreamer_client_for_iOS_LSMPNDeviceStatus_h
#define Lightstreamer_client_for_iOS_LSMPNDeviceStatus_h


/**
 * The LSMPNDeviceStatus enum contains the possible statuses of an MPN device. It is returned as the LSMPNStatusInfo#deviceStatus field
 * by the LSMPNSubscription#checkStatus method.
 * <BR/>Its significant values are:<UL>
 * <LI>#LSMPNDeviceStatusActive: the MPN device is active;
 * <LI>#LSMPNDeviceStatusSuspended: the MPN device has been suspended due to invalidation of the device token by APNS Feedback Service.
 * </UL>
 * The #LSMPNDeviceStatusActive value is the common status of an MPN device. Active MPN subscriptions related to this device
 * are sending their mobile push (i.e. remote) notifications as usual.
 * <BR/>The #LSMPNDeviceStatusSuspended status indicates the device is temporarily not active, but it will be reactivated as soon as
 * a device token change is notified to the Server. This status is reversible and usually requires no action. See
 * LSClient#registrationForMPNSucceededWithToken: for more information. Note that a suspended device may be deactivated (and hence deleted) by the Server,
 * if no token change is notified within a timeout period (by default a week).
 * <BR/>The special value #LSMPNDeviceStatusNone is used only when one of the two previous statuses could not be determined. It should never be used under
 * normal conditions, and if it is found as a value of LSMPNStatusInfo#deviceStatus should be considered an error condition.
 * <BR/>Recall that when the last subscription of a device has been deactivated (and hence deleted), the device also is deleted.
 * A call to LSMPNSubscription#checkStatus for a device with no more active or triggered subscriptions will result in an LSPushServerException
 * with error code 45 (device unknown).
 */
typedef enum {
	LSMPNDeviceStatusNone= 0,
	LSMPNDeviceStatusActive= 1,
	LSMPNDeviceStatusSuspended= 9
} LSMPNDeviceStatus;


#endif
