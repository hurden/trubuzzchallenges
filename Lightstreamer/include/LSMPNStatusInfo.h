//
//  LSMPNStatusInfo.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>
#import "LSMPNSubscriptionStatus.h"
#import "LSMPNDeviceStatus.h"


/**
 * The LSMPNStatusInfo class contains additional information about the status of a mobile push
 * notification (MPN) subscription (see LSMPNSubscription#checkStatus) and its related device.
 */
@interface LSMPNStatusInfo : NSObject <NSCopying> {}


#pragma mark -
#pragma mark Initialization

/**
 * Creates and returns an LSMPNStatusInfo object with the specified parameters.
 * <BR/>This factory method is for internal use only.
 *
 * @param subscriptionStatus Status of the MPN subscription (see LSMPNSubscriptionStatus for more information).
 * @param subscriptionStatusDate Date and time when the MPN subscription status has been set.
 * @param deviceStatus Status of the MPN device (see LSMPNDeviceStatus for more information).
 * @param deviceStatusDate Date and time when the MPN device status has been set.
 */
+ (LSMPNStatusInfo *) mpnStatusInfoWithSubscriptionStatus:(LSMPNSubscriptionStatus)subscriptionStatus date:(NSDate *)subscriptionStatusDate deviceStatus:(LSMPNDeviceStatus)deviceStatus date:(NSDate *)deviceStatusDate;

/**
 * Initializes an LSMPNStatusInfo object with the specified parameters.
 * <BR/>This initializer is for internal use only.
 *
 * @param subscriptionStatus Status of the MPN subscription (see LSMPNSubscriptionStatus for more information).
 * @param subscriptionStatusDate Date and time when the MPN subscription status has been set.
 * @param deviceStatus Status of the MPN device (see LSMPNDeviceStatus for more information).
 * @param deviceStatusDate Date and time when the MPN device status has been set.
 */
- (id) initWithSubscriptionStatus:(LSMPNSubscriptionStatus)subscriptionStatus date:(NSDate *)subscriptionStatusDate deviceStatus:(LSMPNDeviceStatus)deviceStatus date:(NSDate *)deviceStatusDate;


#pragma mark -
#pragma mark Properties

/**
 * Status of the MPN subscription (see LSMPNSubscriptionStatus for more information).
 */
@property (nonatomic, readonly) LSMPNSubscriptionStatus subscriptionStatus;

/**
 * Date and time when the MPN subscription status has been set.
 */
@property (nonatomic, readonly) NSDate *subscriptionStatusDate;

/**
 * Status of the MPN device (see LSMPNDeviceStatus for more information).
 */
@property (nonatomic, readonly) LSMPNDeviceStatus deviceStatus;

/**
 * Date and time when the MPN device status has been set.
 */
@property (nonatomic, readonly) NSDate *deviceStatusDate;


@end
