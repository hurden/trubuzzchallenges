//
//  LSMPNSubscriptionStatus.h
//  Lightstreamer client for iOS
//

#ifndef Lightstreamer_client_for_iOS_LSMPNStatus_h
#define Lightstreamer_client_for_iOS_LSMPNStatus_h


/**
 * The LSMPNSubscriptionStatus enum contains the possible statuses of an MPN subscription. It is returned as the LSMPNStatusInfo#subscriptionStatus field
 * by the LSMPNSubscription#checkStatus method.
 * It is also used to specify a filter for calls to LSClient#deactivateMPNsWithStatus: and LSClient#inquireMPNsWithStatus:.
 * <BR/>Its significant values are:<UL>
 * <LI>#LSMPNSubscriptionStatusActive: the MPN subscription is active (if it has a trigger expression, it has not triggered yet);
 * <LI>#LSMPNSubscriptionStatusTriggered: the MPN subscription is active and it has a trigger expression that has already triggered.
 * </UL>
 * The #LSMPNSubscriptionStatusActive value is the common status of an MPN subscription. If it has no trigger expression,
 * it means the MPN subscription is sending its mobile push (i.e. remote) notifications as usual. If it has a trigger expression, 
 * it means the MPN subscription is waiting for it to evaluate as true.
 * <BR/>The #LSMPNSubscriptionStatusTriggered status indicates the MPN subscription has already sent its only notification and may safely be deactivated.
 * See LSMPNInfo#triggerExpression for more information.
 * <BR/>The special value #LSMPNSubscriptionStatusNone is used as an argument to LSClient#deactivateMPNsWithStatus: and LSClient#inquireMPNsWithStatus:
 * to indicate that no filter on the status must be applied, thus meaning "all MPN subscriptions". In this case the call is equivalent to, respectively,
 * LSClient#deactivateAllMPNs and LSClient#inquireAllMPNs.
 * <BR/>Recall that when a subscription has been deactivated it has also been deleted. Hence, there isn't a corresponding state:
 * a call to LSMPNSubscription#checkStatus for such a subscription will result in an LSPushServerException with error code 46 (subscription unknown)
 * or 45 (device unknown, if no more subscriptions exist for this device and application).
 */
typedef enum {
	LSMPNSubscriptionStatusNone= 0,
	LSMPNSubscriptionStatusActive= 1,
	LSMPNSubscriptionStatusTriggered= 2
} LSMPNSubscriptionStatus;


#endif
