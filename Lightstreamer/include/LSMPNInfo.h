//
//  LSMPNInfo.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>

#define AUTO_BADGE_VALUE         (@"AUTO")


@class LSTableInfo;

/**
 * The LSMPNInfo class contains the table and format specification of mobile push notifications (MPN)
 * to be sent on the update of an MPN subscription. The only required parameter is #tableInfo,
 * other parameters are optional and may be left to nil.
 * Common Apple's APNS Services restrictions apply, such as maximum total length of 2 Kbytes.
 * <BR/>Every parameter value may include references to table update fields by position, e.g.:<UL>
 * <LI>"Stock value of $[1] is now $[2]"
 * <LI>"Latest news: $[1] of $[2]"
 * </UL>
 * If the Metadata Adapter to which the table belongs is a subclass of LiteralBasedProvider (or of an equivalent
 * class), the value may also include field references by name, e.g.:<UL>
 * <LI>"Stock value of ${stock_name} is now ${last_price}"
 * <LI>"Latest news: ${title} of ${time}"
 * </UL>
 * In this case common metadata rules apply: field names are checked against the Metadata Adapter and as such they
 * must be consistent with the schema and group specified on the LSTableInfo corresponding to the MPN subscription.
 * <BR>A Server-managed special value may also be referenced:<UL>
 * <LI>${LS_MPN_subscription_ID} is the ID of the MPN subscription generating the mobile push notification.
 * </UL>
 * Field and special value references will be expanded to their corresponding content before mobile push
 * notification delivery, so that the user will actually receive a human-readable notification (e.g. "Stock 
 * value of Apple Inc. is now 100").
 * <BR/>Objects of this class may be created and passed to LSClient on calls like LSClient#activateMPN:coalescing:,
 * but may also be returned as part of an LSMPNSubscription by calls like LSClient#inquireMPNsWithStatus:.
 * In this case some properties may not be restored to the value they had when the LSMPNInfo objects have been originally 
 * submitted. In particular field references may have all been transformed in positional format (e.g. "$[2]" instead of "${last_price}").
 * <BR/>Note that instances of this class may be made unmodifiable when stored inside another object (e.g. an
 * LSMPNSubscription instance). In this case, trying to change one of the properties causes an LSPushClientException
 * to be raised.
 * <BR/>For more information on the meaning and use of each parameter please refer to Apple's Local and Push
 * Notification Programming Guide.
 */
@interface LSMPNInfo : NSObject <NSCopying> {}


#pragma mark -
#pragma mark Initialization

/**
 * Creates and returns an LSMPNInfo object with the specified parameters.
 * 
 * @param tableInfo Contains the specification of the table to be subscribed. A copy of the object is stored internally.
 * @param sound Specifies the sound to be used with the mobile push notification (e.g. "Default").
 * @param badge Specifies the badge to be used with the mobile push notification (e.g. "1"). If the special
 * value "AUTO" is used, the badge will be assigned automatically (see the #badge property for details).
 * @param format Specifies the text to be used with the mobile push notification (e.g. "You have new mail",
 * or "Stock value of $[1] is now $[2]", or even "Latest news: ${title} of ${time}").
 * 
 * @return The LSMPNInfo object.
 */
+ (LSMPNInfo *) mpnInfoWithTableInfo:(LSTableInfo *)tableInfo sound:(NSString *)sound badge:(NSString *)badge format:(NSString *)format;

/**
 * Creates and returns an LSMPNInfo object with the specified parameters.
 *
 * @param tableInfo Contains the specification of the table to be subscribed. A copy of the object is stored internally.
 * @param sound Specifies the sound to be used with the mobile push notification (e.g. "Default").
 * @param badge Specifies the badge to be used with the mobile push notification (e.g. "1"). If the special
 * value "AUTO" is used, the badge will be assigned automatically (see the #badge property for details).
 * @param localizedFormatKey Specifies the key of a localized string to be used as the text of the mobile
 * push notification (e.g. "YOU_HAVE_NEW_MAIL").
 * @param arguments Specifies the list of arguments to be used to format the localized format key,
 * in case it contains formatting elements (e.g. "${title}" and "${time}").
 *
 * @return The LSMPNInfo object.
 */
+ (LSMPNInfo *) mpnInfoWithTableInfo:(LSTableInfo *)tableInfo sound:(NSString *)sound badge:(NSString *)badge localizedFormatKey:(NSString *)localizedFormatKey arguments:(NSArray *)arguments;

/**
 * Creates and returns an LSMPNInfo object with the specified parameters.
 *
 * @param tableInfo Contains the specification of the table to be subscribed. A copy of the object is stored internally.
 * @param sound Specifies the sound to be used with the mobile push notification (e.g. "Default").
 * @param badge Specifies the badge to be used with the mobile push notification (e.g. "1"). If the special
 * value "AUTO" is used, the badge will be assigned automatically (see the #badge property for details).
 * @param localizedActionKey Specifies the key of a localized string to be used as the action button of
 * the mobile push notification (e.g. "OPEN" or "VIEW").
 * @param localizedFormatKey Specifies the key of a localized string to be used as the text of the mobile
 * push notification (e.g. "YOU_HAVE_NEW_MAIL").
 * @param arguments Specifies the list of arguments to be used to format the localized format key,
 * in case it contains formatting elements (e.g. "${title}" and "${time}").
 *
 * @return The LSMPNInfo object.
 */
+ (LSMPNInfo *) mpnInfoWithTableInfo:(LSTableInfo *)tableInfo sound:(NSString *)sound badge:(NSString *)badge localizedActionKey:(NSString *)localizedActionKey localizedFormatKey:(NSString *)localizedFormatKey arguments:(NSArray *)arguments;

/**
 * Creates and returns an LSMPNInfo object with the specified parameters.
 *
 * @param tableInfo Contains the specification of the table to be subscribed. A copy of the object is stored internally.
 * @param sound Specifies the sound to be used with the mobile push notification (e.g. "Default").
 * @param badge Specifies the badge to be used with the mobile push notification (e.g. "1"). If the special
 * value "AUTO" is used, the badge will be assigned automatically (see the #badge property for details).
 * @param localizedActionKey Specifies the key of a localized string to be used as the action button of
 * the mobile push notification (e.g. "OPEN" or "VIEW").
 * @param category Specifies the category of the mobile push notification, to be used by the receiving application
 * to present appropriate action buttons (e.g. "MAIL_MESSAGE"). Used only by devices with iOS 8 and greater.
 * @param localizedFormatKey Specifies the key of a localized string to be used as the text of the mobile
 * push notification (e.g. "YOU_HAVE_NEW_MAIL").
 * @param arguments Specifies the list of arguments to be used to format the localized format key,
 * in case it contains formatting elements (e.g. "${title}" and "${time}").
 *
 * @return The LSMPNInfo object.
 */
+ (LSMPNInfo *) mpnInfoWithTableInfo:(LSTableInfo *)tableInfo sound:(NSString *)sound badge:(NSString *)badge localizedActionKey:(NSString *)localizedActionKey category:(NSString *)category localizedFormatKey:(NSString *)localizedFormatKey arguments:(NSArray *)arguments;


#pragma mark -
#pragma mark Properties

/**
 * Contains the specification of the table to be subscribed. May be an LSTableInfo or LSExtendedTableInfo.
 * <BR/>This parameter is mandatory and must be set.
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, copy) LSTableInfo *tableInfo;

/**
 * Specifies the sound to be used with the mobile push notification (for example: "Default").
 * <BR/>The content of this property may be subject to length restrictions (see the Server's MPN database configuration for more information).
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, copy) NSString *sound;

/**
 * Specifies the badge to be used with the mobile push notification (for example: "1").
 * If the special value "AUTO" is used, the badge will be assigned automatically as a progressive counter
 * of all notifications originated by all MPN subscriptions with the "AUTO" value, on a per-device
 * and per-application basis. The counter can also be reset at any time by invoking
 * LSClient#applicationMPNBadgeReset:.
 * <BR/>Note that the badge parameter is per-subscription, like other parameters, so an uncoordinated
 * use between multiple MPN subscriptions may lead to conflicting numbers on the application's icon.
 * On the other hand, using "AUTO" for all subscriptions ensures that no such conflicts may arise. 
 * <BR/>The content of this property may be subject to length restrictions (see the Server's MPN database configuration for more information).
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, copy) NSString *badge;

/**
 * Specifies the flag to signal the availability of new content to a Newsstand application (for example: "1").
 * <BR/>The content of this property may be subject to length restrictions (see the Server's MPN database configuration for more information).
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, copy) NSString *contentAvailable;

/**
 * Specifies the launch image to be shown when opening the mobile push notification (for example: "Default@2x.png").
 * <BR/>The content of this property may be subject to length restrictions (see the Server's MPN database configuration for more information).
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, copy) NSString *launchImage;

/**
 * Specifies the key of a localized string to be used as the action button of
 * the mobile push notification (for example: "OPEN" or "VIEW").
 * <BR/>The content of this property may be subject to length restrictions (see the Server's MPN database configuration for more information).
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, copy) NSString *localizedActionKey;

/**
 * Specifies the category of the mobile push notification, to be used by the receiving application
 * to present appropriate action buttons (for example: "MAIL_MESSAGE").
 * <BR/>Note: only devices with iOS 8 or greater can make use of this property. For compatibility with devices with 
 * iOS up to 7.x use #localizedActionKey. Both properties may be used at the same time.
 * <BR/>The content of this property may be subject to length restrictions (see the Server's MPN database configuration for more information).
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, copy) NSString *category;

/**
 * Specifies the text to be used with the mobile push notification (for example: "You have new mail",
 * or "Stock value of $[1] is now $[2]", or even "Latest news: ${title} of ${time}").
 * <BR/>Cannot be used if #localizedFormatKey has been supplied.
 * <BR/>The content of this property may be subject to length restrictions (see the Server's MPN database configuration for more information).
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, copy) NSString *format;

/**
 * localizedFormatKey Specifies the key of a localized string to be used as the text of the mobile
 * push notification (for example: "YOU_HAVE_NEW_MAIL").
 * <BR/>Cannot be used if #format has been supplied.
 * <BR/>The content of this property may be subject to length restrictions (see the Server's MPN database configuration for more information).
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, copy) NSString *localizedFormatKey;

/**
 * Specifies the list of arguments to be used to format the localized format string
 * obtained from localizedFormatKey, in case it contains formatting elements.
 * For example, if the localized string is "Latest news: %@ of %@", 
 * arguments could be: <CODE>@[ @"${title}", @"${time}" ]</CODE>
 * <BR/>The content of this property may be subject to length restrictions (see the Server's MPN database configuration for more information).
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, copy) NSArray *arguments;

/**
 * Specifies a set of additional key-value pairs to be added to the mobile push notification,
 * for interpretation by the application's code. For example: <CODE>@@{ @"channelName" : @"WorldNews1", @"subscriptionId" : @"${LS_MPN_subscription_ID}" }</CODE>
 * <BR/>The content of this property may be subject to length restrictions (see the Server's MPN database configuration for more information).
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, copy) NSDictionary *customData;

/**
 * Specifies a boolean expression that, when set, will be evaluated against each table update and will
 * act as a trigger to deliver the mobile push notification. The expression must be strictly in Java syntax 
 * (not Objective-C, not Javascript, not everything else) with the addition of field references. At time of evaluation, 
 * the value of referenced fields (e.g. "${last_price}") is included as <CODE>String</CODE> variables, and as such 
 * appropriate type conversion must be considered (e.g. "Double.parseDouble(${last_price}) > 500.0"). 
 * The names of these variables are composed by the prefix "LS_MPN_field" followed by an index. Thus, variable names like "LS_MPN_field1"
 * should be considered reserved and their use avoided in the expression.
 * <BR/>Consider potential impact on Server performance when writing trigger expressions. Since Java code may
 * use classes and methods of the JDK, a trigger may cause CPU hogging or memory exhaustion if not well considered. 
 * For this reason, a Server-side filter may be applied to refuse poorly written (or even maliciously crafted) 
 * trigger expressions. See the Server's MPN notifier configuration file for more information.
 * <BR/>If NO expression is set, each table update will result in a mobile push notification.
 * If an expression IS set, a mobile push notification will result only if the expression
 * evaluates to <CODE>true</CODE> and no other successive push notifications will be sent.
 * <BR/>The content of this property may be subject to length restrictions (see the Server's MPN database configuration for more information).
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, copy) NSString *triggerExpression;


@end
