//
//  LSMode.h
//  Lightstreamer client for iOS
//

#ifndef Lightstreamer_client_for_iOS_LSMode_h
#define Lightstreamer_client_for_iOS_LSMode_h


/**
 * The LSMode enum contains currently supported subscription modes.
 */
typedef enum {
	LSModeRaw= 0,
	LSModeMerge= 1,
	LSModeDistinct= 2,
	LSModeCommand= 3
} LSMode;


#endif
