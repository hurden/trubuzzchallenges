//
//  LSPushServerException.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>
#import "LSException.h"


/**
 * The LSPushServerException class incapsulates exceptions thrown due to a specific error returned by the Server.
 */
@interface LSPushServerException : LSException {}


#pragma mark -
#pragma mark Initialization

/**
 * Creates and raises an LSPushServerException object with specified parameters.
 * 
 * @param errorCode The specific error code returned by the Server.
 * @param reason Reason of the exception.
 *
 * @return The LSPushServerException object.
 */
+ (LSPushServerException *) serverExceptionWithCode:(int)errorCode reason:(NSString *)reason, ...;

/**
 * Initializes an LSPushServerException object with specified parameters.
 * 
 * @param errorCode The specific error code returned by the Server.
 * @param reason Reason of the exception.
 *
 * @return The LSPushServerException object.
 */
- (id) initWithCode:(int)errorCode reason:(NSString *)reason, ...;

/**
 * Initializes an LSPushServerException object with specified parameters.
 * 
 * @param errorCode The specific error code returned by the Server.
 * @param reason Reason of the exception.
 * @param arguments Variable argument list of parameters.
 *
 * @return The LSPushServerException object.
 */
- (id) initWithCode:(int)errorCode reason:(NSString *)reason arguments:(va_list)arguments;


#pragma mark -
#pragma mark Properties

/**
 * The specific error code returned by the Server.
 * See the text protocol documentation for a reference of the error codes
 * that can be issued in response to the operation performed (i.e.
 * stream/poll, subscription, or synchronous sendMessage request).
 * On the other hand, if the error is in response to a MPN request,
 * then the following codes may be received:<UL>
 * <LI> 17: bad Data Adapter name or default Data Adapter not defined for the current Adapter Set
 * <LI> 21: bad Group name
 * <LI> 22: bad Group name for this Schema
 * <LI> 23: bad Schema name
 * <LI> 24: mode not allowed for an Item
 * <LI> 40: mobile push notifications (MPN) are not supported by this Server instance
 * <LI> 41: mobile push notification (MPN) request timed out
 * <LI> 42: mobile push notification (MPN) request specified an invalid platform
 * <LI> 43: mobile push notification (MPN) request specified an invalid application ID
 * <LI> 44: mobile push notification (MPN) request specified an invalid format or syntax in trigger expression
 * <LI> 45: mobile push notification (MPN) request specified an unknown device token
 * <LI> 46: mobile push notification (MPN) request specified an unknown or invalid subscription ID
 * <LI> 47: mobile push notification (MPN) request specified an invalid argument name in format or trigger expression
 * <LI> 48: mobile push notification (MPN) request specified a device token that has been invalidated by APNS Feedback Service
 * <LI> 49: mobile push notification (MPN) request specified a subscription parameter that exceeded maximum length
 * <LI> 50: mobile push notification (MPN) request specified a subscription with no fields or no items
 * <LI> <= 0: the Metadata Adapter has refused the connection or subscription request, the code value is dependent
 * on the specific Metadata Adapter implementation
 * </UL>
 */
@property (nonatomic, readonly) int errorCode;


@end
