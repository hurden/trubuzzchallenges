//
//  LSTableInfo.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>
#import "LSMode.h"


/**
 * The LSTableInfo class contains the specification of a table to be subscribed to Lightstreamer Server.
 * It may be created and passed to LSClient on calls like LSClient#subscribeTable:delegate:useCommandLogic:,
 * but may also be returned as part of an LSMPNInfo by calls like LSClient#inquireMPN:. In this case some of
 * its properties may not be restored to the value they had when the LSTableInfo has been originally submitted, 
 * like #requestedBufferSize and #requestedMaxFrequency.
 * <BR/>Note that instances of this class may be made unmodifiable when stored inside another object (e.g. an
 * LSMPNInfo instance). In this case, trying to change one of the properties causes an LSPushClientException
 * to be raised.
 */
@interface LSTableInfo : NSObject <NSCopying> {
	
@protected
	NSString *_group;
	NSString *_schema;
}


#pragma mark -
#pragma mark Initialization

/**
 * Creates and returns an LSTableInfo object with the specified parameters.
 *
 * @param group The name of the Group of items contained in this table.
 * @param mode The subscription mode for the items contained in this table.
 * @param schema The name of the Schema of fields used in this table.
 * @param dataAdapter The name of the Data Adapter (within the Adapter Set used by the current session) 
 * that supplies all the items in the Group. If it is nil, the "DEFAULT" name is used.
 * @param snapshot Whether or not the snapshot is being asked for the items contained in this table.
 *
 * @return The LSTableInfo object.
 */
+ (LSTableInfo *) tableInfoWithGroup:(NSString *)group mode:(LSMode)mode schema:(NSString *)schema dataAdapter:(NSString *)dataAdapter snapshot:(BOOL)snapshot;


#pragma mark -
#pragma mark Properties

/**
 * The name of the Group of items contained in this table.
 * <BR/>When used with an MPN subscription, the content of this property may be subject to length restrictions (see the Server's MPN database configuration for more information).
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, copy) NSString *group;

/**
 * The subscription mode for the items contained in this table.
 * <BR/>When used with an MPN subscription, only #LSModeMerge and #LSModeDistinct may be used.
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, assign) LSMode mode;

/**
 * The name of the Schema of fields used in this table.
 * <BR/>When used with an MPN subscription, the content of this property may be subject to length restrictions (see the Server's MPN database configuration for more information).
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, copy) NSString *schema;

/**
 * The name of the Adater Set. When LSTableInfo is used for a new table subscription (or mobile
 * push notification subscription) it is taken from the current session and may be left
 * to nil. If set must correspond to the session's Adapter Set.
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, copy) NSString *adapterSet;

/**
 * The name of the Data Adapter (within the Adapter Set used by the current session) 
 * that supplies all the items in the Group. If it is nil, the "DEFAULT" name is used.
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, copy) NSString *dataAdapter;

/**
 * The selector to be applied by the Server to the updates.
 * <BR/>When used with an MPN subscription, this property must be left to nil.
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, copy) NSString *selector;

/**
 * Whether or not the snapshot is being asked for the items contained in this table.
 * <BR/>The default is NO.
 * <BR/>When used with an MPN subscription, this property must be left to NO.
 *
 * @throws LSPushClientException Thrown if snapshot is set for modes other than #LSModeCommand, #LSModeDistinct and #LSModeMerge.
 * Thrown also when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, assign) BOOL snapshot;

/**
 * The position of the first item in the Group to be considered. 0 means no constraint.
 * <BR/>The default is 0.
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, assign) int start;

/**
 * the position of the last item in the Group to be considered. 0 means no constraint.
 * <BR/>The default is 0.
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, assign) int end;

/**
 * The requested length for the snapshot to be received for all the items in the table. If 0, the snapshot length will
 * be determined by the Server.
 * <BR/>NOTE: the snapshot length can be specified only for #LSModeDistinct mode.
 * <BR/>The default is 0.
 * <BR/>When used with an MPN subscription, this property must be left to 0.
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, assign) int requestedDistinctSnapshotLength;

/**
 * the requested size for the Server ItemEventBuffer for all the items in the table. 0 means unlimited buffer. If less than 0, 
 * a 1 element buffer is meant if mode is #LSModeMerge and an unlimited buffer is meant if mode is #LSModeDistinct.
 * However, the real Server buffer size can be limited by the Metadata Adapter.
 * <BR/>NOTE: the buffer size can be specified only for #LSModeMerge and #LSModeDistinct modes.
 * <BR/>It is ignored if unfilteredDispatch is set.
 * <BR/>The default is -1.
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, assign) int requestedBufferSize;

/**
 * The maximum update frequency for all the items in the table. 0.0 means unlimited frequency. It can only be used in order to lower
 * the frequency granted by the Metadata Adapter.
 * <BR/>NOTE:The update frequency can be specified only for #LSModeMerge, #LSModeDistinct and
 * #LSModeCommand mode; in the latter case, the frequency limit only applies to consecutive UPDATE events for the same key value.
 * <BR/>It is ignored if unfilteredDispatch is set.
 * <BR/>The default is 0.
 * <BR/><B>Edition Note:</B> a further global frequency limit is also imposed by the Server, if it is running in Presto, Allegro or 
 * Moderato edition; this specific limit also applies to #LSModeRaw and to unfiltered dispatching.
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, assign) double requestedMaxFrequency;

/**
 * Events for the items in the table are dispatched in an unfiltered way. This setting is stronger than the setting
 * of the maximum update frequency and can lead to the subscription refusal by the Metadata Adapter. It is also stronger than the
 * setting for the buffer size.
 * <BR/>NOTE: the unfiltered dispatching can be specified only for #LSModeMerge, #LSModeDistinct and #LSModeCommand Items mode.
 * <BR/>The default is NO.
 * <BR/>When used with an MPN subscription, this property must be left to NO.
 *
 * @throws LSPushClientException Thrown when trying to change the property if the instance has been made unmodifiable.
 */
@property (nonatomic, assign) BOOL unfilteredDispatch;


@end
