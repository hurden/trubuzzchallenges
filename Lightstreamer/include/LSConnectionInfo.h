//
//  LSConnectionInfo.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>


/**
 * The LSMaxConcurrentSessionsPerServerExceededPolicy enum specifies which action to take when the maximum number of
 * concurrenct streaming connections is exceed. Possible actions are:
 * <UL>
 * <LI>None: no action will be taken, the exceed connection(s) will be automatically suspended by the system until an active
 * connection is closed;</LI>
 * <LI>Use Polling: the exceeding connection(s) will revert to standard polling, thus ensuring that the limit is exceed for shortest
 * possible time and giving a chance to each connection to do its job;</LI>
 * <LI>Block: the exceeding connection(s) will be prevented by raising an exception.</LI>
 * </UL>
 */
typedef enum {
	LSMaxConcurrentSessionsPerServerExceededPolicyNone= 0,
	LSMaxConcurrentSessionsPerServerExceededPolicyUsePolling,
	LSMaxConcurrentSessionsPerServerExceededPolicyBlock
} LSMaxConcurrentSessionsPerServerExceededPolicy;


@class LSConnectionConstraints;

/**
 * The LSConnectionInfo class collects the parameters related to a connection request to Lightstreamer Server.
 */
@interface LSConnectionInfo : NSObject <NSCopying> {}


#pragma mark -
#pragma mark Static properties

/**
 * Returns the maximum number of streaming connections that can be concurrently open to the same Lightstreamer Server end-point (that is: host name and port).
 * <BR/>The default setting in 2.
 */
+ (int) maxConcurrentSessionsPerServer;

/**
 * Sets the maximum number of streaming connections that can be concurrently open to the same Lightstreamer Server end-point (that is: host name and port).
 * <BR/>The change is effective immediately, but active connections that are in excess of this value won't be closed.
 */
+ (void) setMaxConcurrentSessionsPerServer:(int)maxConcurrentSessionsPerServer;


#pragma mark -
#pragma mark Initialization

/**
 * Creates and retuns an LSConnectionInfo object with the specified connection parameters.
 *
 * @param pushServerURL Lightstreamer Server URL, without the path part (i.e.: "https://push.lightstreamer.com")
 * @param pushServerControlURL Lightstreamer Server URL (without the path part) to be used for control requests (i.e.: "http://push.lightstreamer.com:80").
 * It may differ from pushServerUrl because of the protocol. If it is nil, then pushServerURL is used.
 * @param user User name to be supplied to the Server in the connection request.
 * @param password User password to be supplied to the Server in the connection request.
 * @param adapter ID of the Adapter Set to be used to handle all requests in the session. An Adapter Set defines the Metadata Adapter and one or several Data Adapters. 
 * If it is nil, the "DEFAULT" ID will be used.
 *
 * @return The LSConnectionInfo object.
 */
+ (LSConnectionInfo *) connectionInfoWithPushServerURL:(NSString *)pushServerURL pushServerControlURL:(NSString *)pushServerControlURL user:(NSString *)user password:(NSString *)password adapter:(NSString *)adapter;


#pragma mark -
#pragma mark Properties

/**
 * Lightstreamer Server URL, without the path part.
 * <BR/><B>Edition Note:</B> HTTPS connections are not supported by the Server if it runs in Allegro or Moderato edition. 
 */
@property (nonatomic, copy) NSString *pushServerURL;

/**
 * Lightstreamer Server URL (without the path part) to be used for control requests.
 * It may differ from #pushServerURL because of the protocol. If it is nil, then #pushServerURL is used.
 * <BR/><B>Edition Note:</B> HTTPS connections are not supported by the Server if it runs in Allegro or Moderato edition. 
 */
@property (nonatomic, copy) NSString *pushServerControlURL;

/**
 * User name to be supplied to the Server in the connection request.
 */
@property (nonatomic, copy) NSString *user;

/**
 * User password to be supplied to the Server in the connection request.
 */
@property (nonatomic, copy) NSString *password;

/**
 * ID of the Adapter Set to be used to handle all requests in the session. An Adapter Set defines the Metadata Adapter and one or several Data Adapters. 
 * If it is nil, the "DEFAULT" ID will be used.
 */
@property (nonatomic, copy) NSString *adapter;

/**
 * If YES, enables the Stream-sense feature, which means that in case a streaming connection can't ben opened it will
 * automatically failback to polling mode.
 * <BR/>The default setting is YES.
 */
@property (nonatomic, assign) BOOL streamSenseEnabled;

/**
 * If YES, enables the automatic handling of the server address in order to bypass the load balancer server (if any) after the
 * first connection and allow the cleanest communication possible between Lightstreamer client and server.
 * <BR/>The default setting is YES.
 */
@property (nonatomic, assign) BOOL controlLinkHandlingEnabled;

/**
 * Constraints to be applied on the overall data flow from the Connection. The specified constraints can only be used in order to restrict
 * the constraints set by Lightstreamer Server Metadata Adapter.
 * If it is nil, no constraints will be specified.
 */
@property (nonatomic, copy) LSConnectionConstraints *constraints;

/**
 * Timeout for activity checks, expressed in seconds. If, after a warning that the connection is stalled (see #probeWarningSecs)
 * no data has been received on the stream connection for this further time, then the connection is forcibly closed.
 * <BR/>The default setting is 3 seconds.
 */
@property (nonatomic, assign) NSTimeInterval probeTimeoutSecs;

/**
 * Timeout for activity check warnings, expressed in seconds. If no data is being received on the stream connection and a keepalive
 * message is late for this time, then a notification is issued to the ConnectionDelegate that the connection is stalled.
 * <BR/>After the notification, if some data or keepalive is received before #probeTimeoutSecs elapses, then a notification
 * is issued to the ConnectionListener that the activity is normal again; otherwise the connection is forcibly closed.
 * <BR/>The default setting is 2 seconds.
 */
@property (nonatomic, assign) NSTimeInterval probeWarningSecs;

/**
 * Keepalive time requested for the stream connection, in seconds. After an inactivity of this length on the stream connection, a keepalive
 * signal is sent by the Server. If it is 0, the keepalive time is decided by the Server. If it is too small, the Server may use a different keepalive time as well.
 * <BR/>The default setting is 0.
 */
@property (nonatomic, assign) NSTimeInterval keepaliveSecs;

/**
 * The maximum elapsed time in seconds allowed for a successful reconnection attempt. Silent reconnections are performed in case
 * of response content length filling or if polling behaviour has been requested.
 * <BR/>NOTE: The first connection is not checked against this timeout; this can be done by the user by observing the elapsed time in the
 * openConnection call.
 * <BR/>The default setting is 5 seconds.
 */
@property (nonatomic, assign) NSTimeInterval reconnectionTimeoutSecs;

/**
 * The maximum elapsed time in seconds allowed for the first successful streaming connection attempt on a streaming session.
 * The timeout check is performed only when the Stream-sense feature is enabled, i.e. #streamSenseEnabled is YES;
 * if it fails, the current attempt will be discarded and a new session opening will be tried in smart polling mode.
 * <BR/>NOTE: In case the connection is not possible at all, the polling attempt may also hang. By observing the elapsed time in the openConnection call,
 * the user can do an overall timeout checking that does not depend on the outcome of single attempts.
 * <BR/>The default setting is 5 seconds.
 */
@property (nonatomic, assign) NSTimeInterval streamingTimeoutSecs;

/**
 * The number of seconds to wait since a disconnection before trying a new connection, from the second reconnection attempt on. 
 * This value is not applied to the first reconnection attempt, see #firstRetryMaxDelaySecs.
 * <BR/>The default setting is 2 seconds.
 */
@property (nonatomic, assign) NSTimeInterval retryDelaySecs;

/**
 * The maximum number of seconds to wait since a disconnection before trying a new connection, for the first reconnection
 * attempt only. The actual delay is a randomized value between 0.0 and this value. This randomization helps avoid a connection
 * bottleneck on the Server, that could happen if all clients tried to reconnect exactly at the same time. For the delay
 * applied from the second attempt on see #retryDelaySecs.
 * <BR/>The default setting is 0.1 seconds.
 */
@property (nonatomic, assign) NSTimeInterval firstRetryMaxDelaySecs;

/**
 * The length to be used by the Server for the response body on a stream connection (a minimum length, however, is ensured by the server).
 * After the content length exhaustion, the connection will be closed and a new connection will be automatically reopened.
 * If it is 0, the length is decided by the Server.
 * <BR/>The default setting is set to a very high value (50 millions).
 */
@property (nonatomic, assign) int contentLength;

/**
 * Time between the end of a poll connection and the start of the next one (in seconds), if polling behaviour has been specified.
 * Zero is a legal value, although the Server may decide to delay request fulfilling if request frequency is too high.
 * <BR/>NOTE: since the Server needs to keep a session active between subsequent poll connections, the Server may force the library to
 * use a polling time shorter than requested.
 * <BR/>The default setting is 0.
 */
@property (nonatomic, assign) NSTimeInterval pollingSecs;

/**
 * Maximum time the Server is allowed to wait for updates in case none are available at connection time and a poll connection has been
 * requested. A zero value leads to a classical polling behaviour, while a nonzero value leads to an "asynchronous polling", also known as
 * smart polling, behaviour. Typically, a nonzero value for this attribute is associated with a zero value for the #pollingSecs property.
 * <BR/>NOTE: since the Server, while waiting for an update, needs to keep a socket and a session active, even if the client has already closed
 * the connection on its side, the Server may force a shorter wait.
 * <BR/>The default setting is 30 seconds.
 */
@property (nonatomic, assign) NSTimeInterval pollingIdleSecs;

/**
 * If set to YES, data streaming is simulated through a smart polling mechanism, that is a sequence of synchronous connections to the Server,
 * each of which will receive the currently available updates, or wait until one is available.
 * <BR/>The use of polling can become necessary in cases where the connection stream is not delivered in real time because of some router or firewall
 * or antivirus. Even when this flag is set to NO, however, the library may detect those cases and automatically attempt to resort to a smart polling
 * connection, provided that enableStreamSense is set to YES.
 * <BR/>The default setting is NO.
 */
@property (nonatomic, assign) BOOL isPolling;

/**
 * Sets the action to take when the maximum number of streaming connections is exceeded, as specified on the #maxConcurrentSessionsPerServer property.
 * See the corresponding LSMaxConcurrentSessionsPerServerExceededPolicy enum for a list of possible actions.
 * <BR/>The default action is Block.
 */
@property (nonatomic, assign) LSMaxConcurrentSessionsPerServerExceededPolicy maxConcurrentSessionsPerServerExceededPolicy;


@end
