//
//  LSConnectionDelegate.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>


@class LSClient;
@class LSException;
@class LSPushUpdateException;
@class LSPushServerException;
@class LSPushConnectionException;
@class LSMPNTokenChangeInfo;

/**
 * The LSConnectionDelegate protocol receives notifications of connection activity and errors.
 * <BR/>Note: almost all notifications are sent in sequence on a dedicated thread. Only notifications
 * regarding client authentication and request decoration are called directly from the session 
 * thread. See #clientConnection:willSendRequestForAuthenticationChallenge: and
 * #clientConnection:isAboutToSendURLRequest: for more information.
 */
@protocol LSConnectionDelegate <NSObject>

@optional


#pragma mark -
#pragma mark Session status events

/**
 * Notification that the session has been successfully opened. This notification is 
 * received just once per session, i.e. in a polled connection will be received only 
 * once. Anyway, it may be received more than once if the LSClient has to reconnect due
 * to external reasons (e.g. connection drops).
 *
 * @param client The LSClient object that handles the connection.
 * @param polling Tells if the session is in streaming mode or polling mode.
 */
- (void) clientConnection:(LSClient *)client didStartSessionWithPolling:(BOOL)polling;

/**
 * Notification that the connection received new bytes from the Server.
 *
 * @param client The LSClient object that handles the connection.
 * @param bytesLength The number of bytes received
 */
- (void) clientConnection:(LSClient *)client didReceiveNewBytes:(int)bytesLength;

/**
 * Notification that the connection did enter or did exit a stalled state. "Stalled" means that
 * the LSClient object did not receive any new bytes for more then the probeWarningSecs specified
 * on the LSConnectionInfo object when opening the connection. It may be a simptom of an
 * impending connection drop.
 *
 * @param client The LSClient object that handles the connection.
 * @param warningStatus YES if the connection did enter a stalled state, NO otherwise.
 */
- (void) clientConnection:(LSClient *)client didChangeActivityWarningStatus:(BOOL)warningStatus;


#pragma mark -
#pragma mark Connection status events

/**
 * Notification that the connection has been established. This notification is
 * received each time the LSClient reconnects automatically, even if on the same
 * session. I.e., in a polled connection will be received at each poll cycle. It 
 * may also be received more than once also if the LSClient has to reconnect due
 * to external reasons (e.g. connection drops).
 *
 * @param client The LSClient object that handles the connection.
 */
- (void) clientConnectionDidEstablish:(LSClient *)client;

/**
 * Notification that the connection did close. It may be received only after a call to 
 * closeConnection on the LSClient object.
 *
 * @param client The LSClient object that handles the connection.
 */
- (void) clientConnectionDidClose:(LSClient *)client;

/**
 * Notification that the connection did end due to a Server-side decision. When
 * this notification is received the LSClient will stop trying to reconnect automatically.
 * See the "END" message in the text protocol documentation for details on the possible causes.
 *
 * @param client The LSClient object that handles the connection.
 * @param cause Specifies the cause that led the Server to close the connection.
 */
- (void) clientConnection:(LSClient *)client didEndWithCause:(int)cause;


#pragma mark -
#pragma mark Connection error events

/**
 * Notification that a block of data received from the Server could not be correctly interpreted.
 *
 * @param client The LSClient object that handles the connection.
 * @param error The specific exception that explains the error.
 */
- (void) clientConnection:(LSClient *)client didReceiveDataError:(LSPushUpdateException *)error;

/**
 * Notification that an unrecoverable exception signaled by the Server has been received.
 * Unless the connection is manually closed, the LSClient will automatically try to
 * reconnect.
 *
 * @param client The LSClient object that handles the connection.
 * @param failure The specific exception that has been received.
 */
- (void) clientConnection:(LSClient *)client didReceiveServerFailure:(LSPushServerException *)failure;

/**
 * Notification that an unrecoverable connection error has been detected. Unless the
 * connection is manually closed, the LSClient will automatically try to reconnect.
 *
 * @param client The LSClient object that handles the connection.
 * @param failure The specific error that has been detected.
 */
- (void) clientConnection:(LSClient *)client didReceiveConnectionFailure:(LSPushConnectionException *)failure;


#pragma mark -
#pragma mark Events related to mobile push notifications

/**
 * Notification that the device token has been successully changed on the Server.
 * The change attempt is the consequence of a previous invocation of LSClient#registrationForMPNSucceededWithToken:.
 * If the device token had been invalidated by the APNS Feedback Service, it is now valid again and
 * all associated MPN subscriptions have been automatically reactivated from suspended state.
 *
 * @param client The LSClient object that handles the connection.
 * @param info Additional information about the device token change (such as the number of reactivated MPN subscriptions).
 */
- (void) clientConnection:(LSClient *)client didSucceedChangingDeviceTokenOnServerWithInfo:(LSMPNTokenChangeInfo *)info;

/**
 * Notification that the device token change has been tried on the Server and failed.
 * The change attempt is the consequence of a previous invocation of LSClient#registrationForMPNSucceededWithToken:.
 * Note that the token change may fail in two situations that not necessarily denote an error condition:<UL>
 * <LI>if there were no active MPN subscriptions related to this device and application;
 * <LI>if too much time has passed since the old device token was invalidated by APNS Feedback Service.
 * </UL>
 * In both cases, the error reported is an LSPushServerException with error code 45 (device unknown).
 * 
 * @param client The LSClient object that handles the connection.
 * @param error The specific error that caused the device token change to fail.
 */
- (void) clientConnection:(LSClient *)client didFailChangingDeviceTokenOnServerWithError:(LSException *)error;

/**
 * Notification that the application's icon badge has been successfully reset on the Server.
 * The reset attempt is the consequence of a previous invocation of LSClient#applicationMPNBadgeReset:.
 *
 * @param client The LSClient object that handles the connection.
 */
- (void) clientConnectionDidSucceedResettingBadgeOnServer:(LSClient *)client;

/**
 * Notification that an application's icon badge reset has been tried on the server and failed.
 * The reset attempt is the consequence of a previous invocation of LSClient#applicationMPNBadgeReset:.
 *
 * @param client The LSClient object that handles the connection.
 * @param error The specific error that caused the badge reset to fail.
 */
- (void) clientConnection:(LSClient *)client didFailResettingBadgeOnServerWithError:(LSException *)error;


#pragma mark -
#pragma mark Other events for authentication and request decoration

/**
 * Notification that the LSClient's underlying connection is going to request authentication for a challenge in
 * order to proceed. If the delegate implements this method, the connection will suspend until <CODE>[challenge sender]</CODE>
 * is called with one of the following methods: <UL>
 * <LI><CODE>useCredential:forAuthenticationChallenge:</CODE>, 
 * <LI><CODE>continueWithoutCredentialForAuthenticationChallenge:</CODE>,
 * <LI><CODE>cancelAuthenticationChallenge:</CODE>,
 * <LI><CODE>performDefaultHandlingForAuthenticationChallenge:</CODE> or
 * <LI><CODE>rejectProtectionSpaceAndContinueWithChallenge:</CODE>.
 * </UL>
 * If not implemented, the default behavior will call <CODE>performDefaultHandlingForAuthenticationChallenge:</CODE>
 * on the <CODE>[challenge sender]</CODE>.
 * <BR>Note: this notification is called directly from the session thread. The method implementation should be
 * fast and nonblocking. Any slow operations should have been performed in advance.
 *
 * @param client The LSClient object that handles the connection.
 * @param challenge The challenge that the client must authenticate in order to proceed with its request.
 */
- (void) clientConnection:(LSClient *)client willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge;

/**
 * Notification that the LSClient is going to submit an URL request. It gives a chance to decorate
 * the request before it is sent to the server, e.g. to set a specific header field or cookie policy.
 * <BR>Note: this notification is called directly from the session thread. The method implementation should be
 * fast and nonblocking. Any slow operations should have been performed in advance.
 * <BR/><B>Warning</B>: implement this notification only if you know what you are doing. Arbitrary changes
 * to the URL request content may lead to connection malfunctioning.
 *
 * @param client The LSClient object that handles the connection.
 * @param urlRequest The URL request being sent.
 */
- (void) clientConnection:(LSClient *)client isAboutToSendURLRequest:(NSMutableURLRequest *)urlRequest;


@end
