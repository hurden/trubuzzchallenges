//
//  LSClient.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>
#import "LSConnectionStatus.h"
#import "LSMPNTokenStatus.h"
#import "LSMPNSubscriptionStatus.h"


@class LSConnectionStateMachine;
@class LSConnectionInfo;
@class LSConnectionConstraints;
@class LSTableInfo;
@class LSExtendedTableInfo;
@class LSSubscribedTableKey;
@class LSSubscriptionConstraints;
@class LSMPNStatusInfo;
@class LSMPNInfo;
@class LSMPNKey;
@class LSMPNSubscription;
@class LSMessageInfo;
@protocol LSConnectionDelegate;
@protocol LSTableDelegate;
@protocol LSMessageDelegate;

@class iSLClient;
@class iSLSession;
@protocol iSLListener;

@class LSThreadPool;


/**
 * The LSClient class incapsulates a single connection to Lightstreamer Server.
 */
@interface LSClient : NSObject {}


#pragma mark -
#pragma mark Initialization

/**
 * Creates and returns a new LSClient object.
 * <BR/>Note: use of multiple LSClient objects is discouraged. See the API docs 
 * home page, section "Using more than one LSClient", for more information.
 *
 * @return The LSClient object.
 */
+ (LSClient *) client;

/**
 * Initializes a new LSClient object.
 * <BR/>Note: use of multiple LSClient objects is discouraged. See the API docs
 * home page, section "Using more than one LSClient", for more information.
 *
 * @return The LSClient object.
 */
- (id) init;


#pragma mark -
#pragma mark Connection management

/**
 * Opens a connection to the Server with the supplied parameters. The Server will initiate a push session
 * for this client through a streaming connection. If a connection is open, it is closed first.
 * <BR/>The method is blocking: it will return only after a connection has been established
 * or the attempt has failed.
 *
 * @param connectionInfo Contains the Server address and the connection parameters. A copy of the object is stored internally.
 * @param delegate Receives notification for connection events.
 * <BR/>Note: the delegate is stored in a weak reference.
 *
 * @throws LSPushServerException Thrown in case the server refuses the connection with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem while connecting to the Server.
 */
- (void) openConnectionWithInfo:(LSConnectionInfo *)connectionInfo delegate:(id <LSConnectionDelegate>)delegate;

/**
 * Opens a connection to the Server with the supplied parameters. 
 * Non-exception-throwing variant of #openConnectionWithInfo:delegate:, see for more information.
 *
 * @param connectionInfo Contains the Server address and the connection parameters. A copy of the object is stored internally.
 * @param delegate Receives notification for connection events.
 * <BR/>Note: the delegate is stored in a weak reference.
 * @param error In case of error, upon return contains an NSError object that describes the problem.
 *
 * @return YES if the connection could be opened, NO if it couldn't. In the latter case, an error is provided.
 */
- (BOOL) openConnectionWithInfo:(LSConnectionInfo *)connectionInfo delegate:(id <LSConnectionDelegate>)delegate error:(NSError * __autoreleasing *)error;

/**
 * Requests new constraints to be applied on the overall data flow from the current connection.
 * The new limits override the limits requested with the connection operation or the last call to this method 
 * (a constraint not set means an unlimited constraint and may override a previous limiting constraint). 
 * They can only be used in order to restrict the constraints set by Lightstreamer Server Metadata Adapter.
 * <BR/>The method is blocking: it will return only after receiving the Server response.
 * 
 * @param connectionConstraints Constraints to be applied.
 * 
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (void) changeConnectionWithConstraints:(LSConnectionConstraints *)connectionConstraints;

/**
 * Requests new constraints to be applied on the overall data flow from the current connection. 
 * Non-exception-throwing variant of #changeConnectionWithConstraints:, see for more information.
 * 
 * @param connectionConstraints Constraints to be applied.
 * @param error In case of error, upon return contains an NSError object that describes the problem.
 *
 * @return YES if the connection could be changed, NO if it couldn't. In the latter case, an error is provided.
 */
- (BOOL) changeConnectionWithConstraints:(LSConnectionConstraints *)connectionConstraints error:(NSError * __autoreleasing *)error;

/**
 * Closes the connection to the Server, if one is open.
 * <BR/>The method is blocking: it will return only after the connection has been closed.
 */
- (void) closeConnection;


#pragma mark -
#pragma mark Subscriptions management

/**
 * Subscribes to a set of items, which share the same schema and other subscription parameters. All item and field names are provided.
 * This requires that a LiteralBasedProvider or equivalent Metadata Adapter is configured on the Server, in order to understand the request.
 * <BR/>The items are not collected as one table, but they are subscribed each in a different table. However, the items are subscribed alltogether
 * with a single connection to the Server. The items unsubscription can be made either in a single operation (through #unsubscribeTables:)
 * or through independent unsubscribe operations.
 * <BR/>Subscribed items are identified to the listener both by position and by by name. If the request fails, no subscriptions have been performed.
 * <BR/>The method is blocking if executed outside of a batch, else it is non-blocking: outside of a batch, it will return only after receiving the Server response.
 * Inside a batch, the method will return immediately with a valid array of handles, but the request will be processed only on batch commit (see #commitBatch).
 *
 * @param tableInfo Contains the specification and request parameters of the items to subscribe to. A copy of the object is stored internally.
 * @param delegate Receives notification of data updates and subscription termination.
 * <BR/>Note: the delegate is stored in a weak reference.
 *
 * @return An array of handles to the subscribed tables, one for each item. The order of the handles reflects the order of the items in the item
 * set description. The handles are needed for unsubscription.
 *
 * @throws LSPushClientException Thrown in case the parameters specify invalid or conflicting values.
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (NSArray *) subscribeItemsWithExtendedInfo:(LSExtendedTableInfo *)tableInfo delegate:(id <LSTableDelegate>)delegate;

/**
 * Subscribes to a set of items, which share the same schema and other subscription parameters.
 * Non-exception-throwing variant of #subscribeItemsWithExtendedInfo:delegate:, see for more information.
 *
 * @param tableInfo Contains the specification and request parameters of the items to subscribe to. A copy of the object is stored internally.
 * @param delegate Receives notification of data updates and subscription termination.
 * <BR/>Note: the delegate is stored in a weak reference.
 * @param error In case of error, upon return contains an NSError object that describes the problem.
 *
 * @return An array of handles to the subscribed tables, one for each item. The order of the handles reflects the order of the items in the item
 * set description. The handles are needed for unsubscription.
 *
 * @throws LSPushClientException Thrown in case the parameters specify invalid or conflicting values.
 */
- (NSArray *) subscribeItemsWithExtendedInfo:(LSExtendedTableInfo *)tableInfo delegate:(id <LSTableDelegate>)delegate error:(NSError * __autoreleasing *)error;

/**
 * Subscribes to a table through the Server. The table is specified by providing item and field names. This requires that
 * a LiteralBasedProvider or equivalent Metadata Adapter is configured on the Server, in order to understand the request.
 * <BR/>The method is blocking if executed outside of a batch, else it is non-blocking: outside of a batch, it will return only after receiving the Server response.
 * Inside a batch, the method will return immediately with a valid handle, but the request will be processed only on batch commit (see #commitBatch).
 *
 * @param tableInfo Contains the specification and request parameters of the items to subscribe to. A copy of the object is stored internally.
 * @param delegate Receives notification of data updates and subscription termination.
 * <BR/>Note: the delegate is stored in a weak reference.
 * @param commandLogic If YES, enables the notification of item updates with "COMMAND logic", This requires that the items are subscribed in #LSModeCommand or
 * behave equivalently; in particular, that the special "key" and "command" fields are included in the schema.
 *
 * @return A handle to the subscribed table, to be used for unsubscription.
 *
 * @throws LSPushClientException Thrown in case the parameters specify invalid or conflicting values.
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (LSSubscribedTableKey *) subscribeTableWithExtendedInfo:(LSExtendedTableInfo *)tableInfo delegate:(id <LSTableDelegate>)delegate useCommandLogic:(BOOL)commandLogic;

/**
 * Subscribes to a table through the Server. The table is specified by providing item and field names.
 * Non-exception-throwing variant of #subscribeTableWithExtendedInfo:delegate:useCommandLogic:, see for more information.
 *
 * @param tableInfo Contains the specification and request parameters of the items to subscribe to. A copy of the object is stored internally.
 * @param delegate Receives notification of data updates and subscription termination.
 * <BR/>Note: the delegate is stored in a weak reference.
 * @param commandLogic If YES, enables the notification of item updates with "COMMAND logic", This requires that the items are subscribed in #LSModeCommand or
 * behave equivalently; in particular, that the special "key" and "command" fields are included in the schema.
 * @param error In case of error, upon return contains an NSError object that describes the problem.
 *
 * @return A handle to the subscribed table, to be used for unsubscription.
 *
 * @throws LSPushClientException Thrown in case the parameters specify invalid or conflicting values.
 */
- (LSSubscribedTableKey *) subscribeTableWithExtendedInfo:(LSExtendedTableInfo *)tableInfo delegate:(id <LSTableDelegate>)delegate useCommandLogic:(BOOL)commandLogic error:(NSError * __autoreleasing *)error;

/**
 * Subscribes to a table through the Server. The table is specified by group name and schema name. Specific item and field names have to be determined by the Metadata Adapter,
 * and are identified to the delegate only by positional information, as item and field names are not known.
 * <BR/>The method is blocking if executed outside of a batch, else it is non-blocking: outside of a batch, it will return only after receiving the Server response.
 * Inside a batch, the method will return immediately with a valid handle, but the request will be processed only on batch commit (see #commitBatch).
 *
 * @param tableInfo Contains the specification and request parameters of the items to subscribe to. A copy of the object is stored internally.
 * @param delegate Receives notification of data updates and subscription termination.
 * <BR/>Note: the delegate is stored in a weak reference.
 * @param commandLogic If YES, enables the notification of item updates with "COMMAND logic", This requires that the items are subscribed in #LSModeCommand or
 * behave equivalently; in particular, that the special "key" and "command" fields are included in the schema.
 *
 * @return A handle to the subscribed table, to be used for unsubscription.
 *
 * @throws LSPushClientException Thrown in case the parameters specify invalid or conflicting values.
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (LSSubscribedTableKey *) subscribeTable:(LSTableInfo *)tableInfo delegate:(id <LSTableDelegate>)delegate useCommandLogic:(BOOL)commandLogic;

/**
 * Subscribes to a table through the Server. The table is specified by group name and schema name. 
 * Non-exception-throwing variant of #subscribeTable:delegate:useCommandLogic:, see for more information.
 *
 * @param tableInfo Contains the specification and request parameters of the items to subscribe to. A copy of the object is stored internally.
 * @param delegate Receives notification of data updates and subscription termination.
 * <BR/>Note: the delegate is stored in a weak reference.
 * @param commandLogic If YES, enables the notification of item updates with "COMMAND logic", This requires that the items are subscribed in #LSModeCommand or
 * behave equivalently; in particular, that the special "key" and "command" fields are included in the schema.
 * @param error In case of error, upon return contains an NSError object that describes the problem.
 *
 * @return A handle to the subscribed table, to be used for unsubscription.
 *
 * @throws LSPushClientException Thrown in case the parameters specify invalid or conflicting values.
 */
- (LSSubscribedTableKey *) subscribeTable:(LSTableInfo *)tableInfo delegate:(id <LSTableDelegate>)delegate useCommandLogic:(BOOL)commandLogic error:(NSError * __autoreleasing *)error;

/**
 * Requests new constraints to be applied on the data flow from the specified subscription.
 * The new limits override the limits requested with the subscription operation or the last call to this method.
 * <BR/>The server may refuse to apply certain constraints under specific conditions,
 * for example when changing the max frequency of an unfiltered subscription. In these cases an LSPushServerException will be raised. 
 * <BR/>The method is blocking if executed outside of a batch, else it is non-blocking: outside of a batch, it will return only after receiving the Server response.
 * Inside a batch, the method will return immediately but the request will be processed only on batch commit (see #commitBatch).
 *
 * @param tableKey Handle to a table as returned by a subscribe call.
 * @param subscriptionConstraints Constraints to be applied.
 * 
 * @throws LSPushClientException Thrown in case the parameters specify invalid or conflicting values.
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (void) changeTableSubscription:(LSSubscribedTableKey *)tableKey withConstraints:(LSSubscriptionConstraints *)subscriptionConstraints;

/**
 * Requests new constraints to be applied on the data flow from the specified subscription.
 * Non-exception-throwing variant of #changeTableSubscription:withConstraints:, see for more information.
 *
 * @param tableKey Handle to a table as returned by a subscribe call.
 * @param subscriptionConstraints Constraints to be applied.
 * @param error In case of error, upon return contains an NSError object that describes the problem.
 *
 * @return YES if the subscription could be changed, NO if it couldn't. In the latter case, an error is provided.
 *
 * @throws LSPushClientException Thrown in case the parameters specify invalid or conflicting values.
 */
- (BOOL) changeTableSubscription:(LSSubscribedTableKey *)tableKey withConstraints:(LSSubscriptionConstraints *)subscriptionConstraints error:(NSError * __autoreleasing *)error;

/**
 * Unsubscribes from a table previously subscribed to the Server. If the request fails, the table has been unsubscribed anyway.
 * <BR/>The method is blocking if executed outside of a batch, else it is non-blocking: outside of a batch, it will return only after receiving the Server response.
 * Inside a batch, the method will return immediately but the request will be processed only on batch commit (see #commitBatch). Note that the unsubscription
 * notification is sent immediately in both cases.
 *
 * @param tableKey Handle to a table as returned by a subscribe call.
 *
 * @throws LSPushClientException Thrown in case the parameter specifies an invalid table handle.
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code. In this case, a zombie subscription will probably remain in the Server.
 * @throws LSPushConnectionException Thrown in case of any other problem. In this case, a zombie subscription will probably remain in the Server.
 */
- (void) unsubscribeTable:(LSSubscribedTableKey *)tableKey;

/**
 * Unsubscribes from a table previously subscribed to the Server.
 * Non-exception-throwing variant of #unsubscribeTable:, see for more information.
 *
 * @param tableKey Handle to a table as returned by a subscribe call.
 * @param error In case of error, upon return contains an NSError object that describes the problem.
 *
 * @return YES if the table could be unsubscribed, NO if it couldn't. In the latter case, an error is provided.
 *
 * @throws LSPushClientException Thrown in case the parameter specifies an invalid table handle.
 */
- (BOOL) unsubscribeTable:(LSSubscribedTableKey *)tableKey error:(NSError * __autoreleasing *)error;

/**
 * Unsubscribes from a set of tables previously subscribed to the Server. The unsubscription requests are sent to the Server in a single connection.
 * If some of the request fails, the tables have been unsubscribed anyway, but some zombie subscriptions may remain in the Server.
 * <BR/>The method is blocking if executed outside of a batch, else it is non-blocking: outside of a batch, it will return only after receiving the Server response.
 * Inside a batch, the method will return immediately but the request will be processed only on batch commit (see #commitBatch). Note that unsubscription
 * notifications are sent immediately in both cases.
 *
 * @param tableKeys Array of handles to tables as returned by one or more subscribe calls.
 */
- (void) unsubscribeTables:(NSArray *)tableKeys;

/**
 * Unsubscribes from a set of tables previously subscribed to the Server.
 * Non-exception-throwing variant of #unsubscribeTables:, see for more information.
 *
 * @param tableKeys Array of handles to tables as returned by one or more subscribe calls.
 * @param error In case of error, upon return contains an NSError object that describes the problem.
 *
 * @return YES if the tables could be unsubscribed, NO if they couldn't. In the latter case, an error is provided.
 */
- (BOOL) unsubscribeTables:(NSArray *)tableKeys error:(NSError * __autoreleasing *)error;


#pragma mark -
#pragma mark Mobile push notifications management

/**
 * Notifies the LSClient that a registration for mobile push (that is: remote) notifications did succeed and specifies the obtained
 * device token. The token is stored on the standard NSUserDefaults storage for subsequent use by other mobile push notification 
 * (MPN) APIs.
 * <BR/>The method checks whether the device token is new, changed or unchanged. Unlike the other, instance-related version
 * of the method, this class-related version can be invoked at any time. If the device token is changed and there is at least
 * one instance of LSClient connected, it will try to update the token on the Server. If there are no instances
 * connected, the device token update on the Server will be scheduled and tried when the first instance connects.
 * The subsequent result of the update try will be delivered through appropriate events of the LSConnectionDelegate.
 * If successful, the device token change may automatically reactivate previously suspended MPN subscriptions. 
 * An MPN subscription may become suspended when APNS Feedback Service reports a device token as invalidated.
 * <BR/>The method is always non-blocking: the outcome of the update will always be delivered asyncronously to the delegate.
 *
 * @param deviceTokenData A device token, as obtained through <CODE>application:didRegisterForRemoteNotificationsWithDeviceToken:</CODE>.
 *
 * @return The device token status, i.e. whether it is new, changed or unchanged since last method call.
 */
+ (LSMPNTokenStatus) registrationForMPNSucceededWithToken:(NSData *)deviceTokenData;

/**
 * Notifies the LSClient that a registration for mobile push (that is: remote) notifications did succeed and specifies the obtained
 * device token. The token is stored on the standard NSUserDefaults storage for subsequent use by other mobile push notification
 * (MPN) APIs.
 * <BR/>The method checks whether the device token is new, changed or unchanged. If it is changed it will try to update
 * the device token on the Server. For this reason the LSClient must be connected when this method is called. 
 * The subsequent result of the update try will be delivered through appropriate events of the LSConnectionDelegate.
 * If successful, the device token change may automatically reactivate previously suspended MPN subscriptions.
 * An MPN subscription may become suspended when APNS Feedback Service reports a device token as invalidated.
 * <BR/>The method is always non-blocking: the outcome of the update will always be delivered asyncronously to the delegate.
 * <BR/>Batching is not effective for this method, it always executes immediately.
 *
 * @param deviceTokenData A device token, as obtained through <CODE>application:didRegisterForRemoteNotificationsWithDeviceToken:</CODE>.
 *
 * @return The device token status, i.e. whether it is new, changed or unchanged since last method call.
 */
- (LSMPNTokenStatus) registrationForMPNSucceededWithToken:(NSData *)deviceTokenData;

/**
 * Notifies the LSClient that the application's icon badge has been reset. This is needed by mobile push notification (MPN) subscriptions 
 * that use the "AUTO" value for the application's icon badge, so that they may correctly restart the badge counting.
 * <BR/>Unlike the other, instance-related version of the method, this class-related version can be invoked at any time.
 * If there is at least one instance of LSClient connected, it will try to reset the badge on the Server. If there are no instances 
 * connected, the reset on the Server will be scheduled and tried when the first instance connects. The subsequent result of the badge 
 * reset try will be delivered through appropriate events of the LSConnectionDelegate.
 * <BR/>The method is always non-blocking: the outcome of the update will always be delivered asyncronously to the delegate.
 */
+ (void) applicationMPNBadgeReset;

/**
 * Notifies the LSClient that the application's icon badge has been reset. This is needed by mobile push notification (MPN) subscriptions
 * that use the "AUTO" value for the application's icon badge, so that they may correctly restart the badge counting.
 * <BR/>The method will try to reset the badge on the Server. For this reason the LSClient must be connected when this method is called. 
 * The subsequent result of the badge reset try will be delivered through appropriate events of the LSConnectionDelegate.
 * <BR/>The method is always non-blocking: the outcome of the update will always be delivered asyncronously to the delegate.
 * <BR/>Batching is not effective for this method, it always executes immediately.
 */
- (void) applicationMPNBadgeReset;

/**
 * Activates a mobile push notification (MPN) subscription for specific table and with a specific format. Once activated,
 * updates on the table will be delivered via mobile push (i.e. remote) notifications in accordance with the format specified in the LSMPNInfo.
 * MPNs can be sent each time the table updates or only one time when a specific trigger passes evaluation.
 * <BR/>An MPN subscription is persistent and it is related only with the current device and application;
 * on the other hand, once activated, it is no longer related with the current Session, which has been used only to submit and authorize it.
 * Therefore the MPN subscription will remain active on the Server until deactivated through LSMPNSubscription#deactivate,
 * #deactivateMPNsWithStatus:, or #deactivateAllMPNs (within the current Session or any other one).
 * In special cases, the subscription may be forcibly deactivated on the Server side.
 * <BR/>Some limitations apply on the table specification:<UL>
 * <LI>#LSModeRaw, #LSModeCommand, unfiltered dispatch and snapshot are not available with MPN subscriptions;
 * <LI>the LSTableInfo#selector field is ignored (but a trigger may be set in the LSMPNInfo#triggerExpression field, see for more information).
 * </UL>
 * If the <CODE>coalescing</CODE> flag is set, two activations with the same Adapter Set, Data Adapter, group, schema and trigger
 * expression are actually considered the same MPN subscription. Activating two such subscriptions will result in the second activation
 * returning an LSMPNSubscription instance related with the first activation (remind that MPN subscriptions are persistent, hence the first
 * subscription may have been issued within a different Session). If other activation parameters, such as LSMPNInfo#format or LSMPNInfo#badge, are specified differently,
 * this will also cause the MPN subscription to be modified. As another consequence, if the subscription has a trigger and is currently
 * in triggered state, by activating again the subscription it will return to the active state (see LSMPNStatus).
 * <BR/>If the <CODE>coalescing</CODE> flag is not set, two activations are always considered different MPN subscriptions, whatever the Adapter Set,
 * Data Adapter, group, schema and trigger expression are set.
 * <BR/>The rationale behind the <CODE>coalescing</CODE> flag is to allow simple apps to always activate their MPN subscriptions 
 * when the app starts, without worrying about receiving the related notifications multiple times because the same subscriptions
 * might have been issued in previous runs of the app.
 * <BR/>Note that MPN subscriptions are cached locally, see LSMPNSubscription description for more information.
 * This method always contacts the Server and updates the related MPN subscription in the local cache.
 * <BR/>The method is blocking if executed outside of a batch, otherwise it is non-blocking: outside of a batch, it will return
 * only after receiving the Server response. Inside a batch, the method will return immediately with nil, but the request will be processed
 * only on batch commit (see #commitBatch).
 *
 * @param mpnInfo Contains the table and format specifications of the mobile push notifications to be activated. An unmodifiable copy of the object is stored internally.
 * @param coalescing If set, two activations with the same Adapter Set, Data Adapter, group, schema and trigger
 * expression are actually considered the same MPN subscription. If not set, two such activations are considered
 * different MPN subscriptions.
 *
 * @return The activated MPN subscription, to be used for modification or deactivation (possibly within a different Session).
 * If a batch is pending, nil is returned.
 *
 * @throws LSPushClientException Thrown in case the parameters specify invalid or conflicting values, 
 * or in case the context is invalid (e.g. can't obtain a valid device token or app ID).
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (LSMPNSubscription *) activateMPN:(LSMPNInfo *)mpnInfo coalescing:(BOOL)coalescing;

/**
 * Activates a mobile push notification (MPN) subscription for specific table and with a specific format.
 * Non-exception-throwing variant of #activateMPN:coalescing:, see for more information.
 *
 * @param mpnInfo Contains the table and format specifications of the mobile push notifications to be activated. An unmodifiable copy of the object is stored internally.
 * @param coalescing If set, two activations with the same Adapter Set, Data Adapter, group, schema and trigger
 * expression are actually considered the same MPN subscription. If not set, two such activations are considered
 * different MPN subscriptions.
 * @param error In case of error, upon return contains an NSError object that describes the problem.
 *
 * @return The activated MPN subscription, to be used for modification or deactivation (possibly within a different Session).
 * If a batch is pending, nil is returned.
 *
 * @throws LSPushClientException Thrown in case the parameters specify invalid or conflicting values, 
 * or in case the context is invalid (e.g. can't obtain a valid device token or app ID).
 */
- (LSMPNSubscription *) activateMPN:(LSMPNInfo *)mpnInfo coalescing:(BOOL)coalescing error:(NSError * __autoreleasing *)error;

/**
 * Inquires the details of a mobile push notification (MPN) subscription with the specified MPN key. An LSMPNSubscription object
 * is returned, with the details about the MPN subscription.
 * <BR/>If the MPN subscription has been forcibly deactivated on the Server, the call will result in a LSPushServerException
 * with error code 46 (subscription unknown) or 45 (device unknown, if no more subscriptions exist for this device and application).
 * <BR/>Note that MPN subscriptions are cached locally, see LSMPNSubscription description for more information.
 * This method always contacts the Server and updates the related MPN subscription in the local cache.
 * <BR/>The method is blocking if executed outside of a batch, else it is non-blocking: outside of a batch, it will return
 * only after receiving the Server response. Inside a batch, the method will return immediately with nil, but the request will be processed
 * only on batch commit (see #commitBatch).
 *
 * @param mpnKey A key to an active MPN subscription (possibly obtained within a different Session).
 *
 * @return An LSMPNSubscription object with the details the specified MPN subscription,
 * or nil if a batch is pending.
 *
 * @throws LSPushClientException Thrown in case the parameter specifies an invalid MPN key, 
 * or in case the context is invalid (e.g. can't obtain a valid device token or app ID).
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (LSMPNSubscription *) inquireMPN:(LSMPNKey *)mpnKey;

/**
 * Inquires the details of a mobile push notification (MPN) subscription with the specified MPN key.
 * Non-exception-throwing variant of #inquireMPN:, see for more information.
 *
 * @param mpnKey A key to an active MPN subscription (possibly obtained within a different Session).
 * @param error In case of error, upon return contains an NSError object that describes the problem.
 *
 * @return An LSMPNSubscription object with the details the specified MPN subscription,
 * or nil if a batch is pending.
 *
 * @throws LSPushClientException Thrown in case the parameter specifies an invalid MPN key, 
 * or in case the context is invalid (e.g. can't obtain a valid device token or app ID).
 */
- (LSMPNSubscription *) inquireMPN:(LSMPNKey *)mpnKey error:(NSError * __autoreleasing *)error;

/**
 * Inquires the details of all mobile push notification (MPN) subscriptions of this device and application. A list of LSMPNSubscription objects is returned,
 * with the details about each MPN subscription.
 * <BR/>Note that MPN subscriptions are cached locally, see LSMPNSubscription description for more information.
 * This method always contacts the Server and updates the whole state of the local cache.
 * <BR/>The method is blocking if executed outside of a batch, else it is non-blocking: outside of a batch, it will return
 * only after receiving the Server response. Inside a batch, the method will return immediately with nil, but the request will be processed
 * only on batch commit (see #commitBatch).
 *
 * @return A list of LSMPNSubscription objects with the details for all MPN subscriptions,
 * or nil if a batch is pending.
 *
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (NSArray *) inquireAllMPNs;

/**
 * Inquires the details of all mobile push notification (MPN) subscriptions of this device and application.
 * Non-exception-throwing variant of #inquireAllMPNs, see for more information.
 *
 * @param error In case of error, upon return contains an NSError object that describes the problem.
 *
 * @return A list of LSMPNSubscription objects with the details for all MPN subscriptions,
 * or nil if a batch is pending.
 */
- (NSArray *) inquireAllMPNs:(NSError * __autoreleasing *)error;

/**
 * Inquires the details of mobile push notification (MPN) subscriptions with the specified status. A list of LSMPNSubscription objects is 
 * returned, with the details about each MPN subscription.
 * <BR/>Note that MPN subscriptions are cached locally, see LSMPNSubscription description for more information.
 * This method always contacts the Server and updates the involved MPN subscriptions in the local cache.
 * <BR/>The method is blocking if executed outside of a batch, else it is non-blocking: outside of a batch, it will return
 * only after receiving the Server response. Inside a batch, the method will return immediately with nil, but the request will be processed
 * only on batch commit (see #commitBatch).
 *
 * @param mpnStatus An MPN subscription status.
 *
 * @return A list of LSMPNSubscription objects with the details for all MPN subscriptions in the specified status for this device and application,
 * or nil if a batch is pending.
 *
 * @throws LSPushClientException Thrown in case the parameter specifies an invalid status,
 * or in case the context is invalid (e.g. can't obtain a valid device token or app ID).
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (NSArray *) inquireMPNsWithStatus:(LSMPNSubscriptionStatus)mpnStatus;

/**
 * Inquires the details of mobile push notification (MPN) subscriptions with the specified status.
 * Non-exception-throwing variant of #inquireMPNsWithStatus:, see for more information.
 *
 * @param mpnStatus An MPN subscription status.
 * @param error In case of error, upon return contains an NSError object that describes the problem.
 *
 * @return A list of LSMPNSubscription objects with the details for all MPN subscriptions in the specified status for this device and application,
 * or nil if a batch is pending.
 *
 * @throws LSPushClientException Thrown in case the parameter specifies an invalid status,
 * or in case the context is invalid (e.g. can't obtain a valid device token or app ID).
 */
- (NSArray *) inquireMPNsWithStatus:(LSMPNSubscriptionStatus)mpnStatus error:(NSError * __autoreleasing *)error;

/**
 * Looks up the details of a mobile push notification (MPN) subscription in the local cache, with the specified MPN key. 
 * If found, An LSMPNSubscription object is returned, with the details about the MPN subscription, otherwise nil is returned.
 * <BR/>For more information on the local cache of MPN subscriptions, see the LSMPNSubscription description.
 * This method never contacts the Server and does not update the local cache.
 * <BR/>The method is not batchable: will always return immediately with the appropriate return value.
 *
 * @param mpnKey A key to an active MPN subscription (possibly obtained within a different Session).
 *
 * @return An LSMPNSubscription object with the details the specified MPN subscription,
 * or nil if the MPN subscription can not be found in the local cache.
 */
- (LSMPNSubscription *) cachedMPNSubscriptionForKey:(LSMPNKey *)mpnKey;

/**
 * Looks up the details of mobile push notification (MPN) subscriptions currently stored in the local cache. A list of 
 * LSMPNSubscription objects is returned, with the details about each MPN subscription.
 * <BR/>For more information on the local cache of MPN subscriptions, see the LSMPNSubscription description.
 * This method never contacts the Server and does not update the local cache.
 * <BR/>The method is not batchable: will always return immediately with the appropriate return value.
 *
 * @return A list of LSMPNSubscription objects with the details for all MPN subscriptions stored in the local cache.
 */
- (NSArray *) cachedMPNSubscriptions;

/**
 * Deactivates all the MPN subscriptions for this device and application. Once deactivated, no more mobile push (i.e. remote)
 * notifications will be sent on table updates. 
 * <BR/>A deactivation has also the effect of deleting the MPN subscriptions,
 * such that any successive operations on them will result in an LSPushServerException with error code 46 (subscription unknown)
 * or 45 (device unknown, if no more subscriptions exist for this device and application).
 * <BR/>Note that MPN subscriptions are cached locally, see LSMPNSubscription description for more information.
 * This method always contacts the Server and updates (clears) the local cache.
 * <BR/>The method is blocking if executed outside of a batch, else it is non-blocking: outside of a batch, it will return
 * only after receiving the Server response. Inside a batch, the method will return immediately but the request will be processed
 * only on batch commit (see #commitBatch).
 *
 * @throws LSPushClientException Thrown in case the context is invalid (e.g. can't obtain a valid device token or app ID).
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (void) deactivateAllMPNs;

/**
 * Deactivates all the MPN subscriptions for this device and application.
 * Non-exception-throwing variant of #deactivateAllMPNs, see for more information.
 *
 * @param error In case of error, upon return contains an NSError object that describes the problem.
 *
 * @return YES if the MPN subscriptions could be deactivated, NO if they couldn't. In the latter case, an error is provided.
 *
 * @throws LSPushClientException Thrown in case the context is invalid (e.g. can't obtain a valid device token or app ID).
 */
- (BOOL) deactivateAllMPNs:(NSError * __autoreleasing *)error;

/**
 * Deactivates mobile push notification (MPN) subscriptions with the specified status. Once deactivated, no more mobile push (i.e. remote)
 * notifications will be sent on table updates. 
 * <BR/>A deactivation has also the effect of deleting the MPN subscriptions,
 * such that any successive operations on them will result in an LSPushServerException with error code 46 (subscription unknown)
 * or 45 (device unknown, if no more subscriptions exist for this device and application).
 * <BR/>Note that MPN subscriptions are cached locally, see LSMPNSubscription description for more information.
 * This method always contacts the Server and updates the involved MPN subscriptions in the local cache.
 * <BR/>The method is blocking if executed outside of a batch, else it is non-blocking: outside of a batch, it will return
 * only after receiving the Server response. Inside a batch, the method will return immediately but the request will be processed
 * only on batch commit (see #commitBatch).
 *
 * @param mpnStatus An MPN subscription status.
 *
 * @throws LSPushClientException Thrown in case the parameter specifies an invalid status,
 * or in case the context is invalid (e.g. can't obtain a valid device token or app ID).
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (void) deactivateMPNsWithStatus:(LSMPNSubscriptionStatus)mpnStatus;

/**
 * Deactivates mobile push notification (MPN) subscriptions with the specified status.
 * Non-exception-throwing variant of #deactivateMPNsWithStatus:, see for more information.
 *
 * @param mpnStatus An MPN subscription status.
 * @param error In case of error, upon return contains an NSError object that describes the problem.
 *
 * @return YES if the MPN subscriptions could be deactivated, NO if they couldn't. In the latter case, an error is provided.
 *
 * @throws LSPushClientException Thrown in case the parameter specifies an invalid status,
 * or in case the context is invalid (e.g. can't obtain a valid device token or app ID).
 */
- (BOOL) deactivateMPNsWithStatus:(LSMPNSubscriptionStatus)mpnStatus error:(NSError * __autoreleasing *)error;


#pragma mark -
#pragma mark Messages management

/**
 * Sends a message to Lightstreamer Server. The message is associated to the current session and is interpreted and managed by the Metadata Adapter related to the session.
 * <BR/>Upon subsequent calls to the method, the sequential management of the messages is guaranteed. However, any message that, for any reason, 
 * doesn't reach the Server can be discarded by the Server if this causes the subsequent message to be kept waiting for longer than a configurable timeout.  
 * A shorter timeout can be associated with the subsequent message itself. A sequence identifier must also be associated with the messages; the sequential management is 
 * restricted to all subsets of messages with the same sequence identifier associated. In case the sequential management is undesired the special 
 * UNORDERED_MESSAGES sequence identifier can be used.
 * <BR/>This method is always non-blocking: it will return immediately with a valid progressive number, but the message will be submitted
 * later. If inside a batch, the message will be submitted on batch commit (see #commitBatch), reporting errors in the commit result
 * and the outcome asynchronously to the delegate. If outside of a batch, reporting both errors and the outcome asynchronously to the delegate.
 *
 * @param messageInfo The message string to be interpreted by the Metadata Adapter, the sequence this message has to be associated with
 * and a delay timeout to be waited by the server for missing messages before considering them lost. A copy of the object is stored internally.
 * @param delegate Receives notification of the outcome of the message request.
 * <BR/>Note: the delegate is stored in a weak reference.
 *
 * @return A number representing the progressive number of the message within its sequence is returned (starting from 1). Note that each time a new session is established
 * the progressive number is reset.
 */
- (int) sendMessage:(LSMessageInfo *)messageInfo delegate:(id <LSMessageDelegate>)delegate;

/**
 * Sends a message to Lightstreamer Server. The message is associated to the current session and is interpreted and managed by the Metadata Adapter related to the session.
 * <BR/>The method is blocking if executed outside of a batch, else it is non-blocking: outside of a batch, it will return only after receiving the Server response.
 * Inside a batch, the method will return immediatly but the request will be processed only on batch commit (see #commitBatch).
 *
 * @param message Any text string, to be interpreted by the Metadata Adapter.
 *
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (void) sendMessage:(NSString *)message;

/**
 * Sends a message to Lightstreamer Server.
 * Non-exception-throwing variant of #sendMessage:, see for more information.
 *
 * @param message Any text string, to be interpreted by the Metadata Adapter.
 * @param error In case of error, upon return contains an NSError object that describes the problem.
 *
 * @return YES if the message could be sent, NO if it couldn't. In the latter case, an error is provided.
 */
- (BOOL) sendMessage:(NSString *)message error:(NSError * __autoreleasing *)error;


#pragma mark -
#pragma mark Request batching

/**
 * Signals that the next requests should be accumulated and sent to Lightstreamer Server with a single connection. The batch will continue to accumulate requests until a
 * commit is requested. When the connetion is closed, any ongoing batch that has not been committed will be automatically aborted.
 * <BR/>Note that subscription requests and message requests are batched separately: all subscriptions will be executed first, followed by messages.
 * <BR/>Note also that batches cannot be nested: there is at most one and only batch ongoing for a given LSClient object.
 *
 * @throws LSPushConnectionException Thrown if the client is not connected.
 */
- (void) beginBatch;

/**
 * Signals that the next requests should be accumulated and sent to Lightstreamer Server with a single connection.
 * Non-exception-throwing variant of #beginBatch, see for more information.
 *
 * @param error In case of error, upon return contains an NSError object that describes the problem.
 *
 * @return YES if the batch could be opened, NO if it couldn't. In the latter case, an error is provided.
 */
- (BOOL) beginBatch:(NSError * __autoreleasing *)error;

/**
 * Commits the current batch. Pending requests are submitted to Lightstreamer Server in two successive blocks: table requests (including table subscription and
 * unsubscriptions, MPN subscription activations, deactivations and inquiries, etc.) are sent first, followed by message requests. Within each block, request
 * ordering is maintained. Results are accumulated and returned in submission order. If the outcome is positive an appropriate object is added
 * (i.e. <CODE>NSNull</CODE> for requests with no return values, or the appropriate object for requests with specific return values, such as 
 * #activateMPN:coalescing:), otherwise the appropriate exception is added. The implementation guarantees an outcome for each request.
 * <BR/>For example, if your batch is made up of the following requests:<OL>
 * <LI>#subscribeTable:delegate:useCommandLogic:
 * <LI>#sendMessage:
 * <LI>#activateMPN:coalescing:
 * <LI>#sendMessage:
 * </OL>
 * they are submitted in this order:<OL>
 * <LI>#subscribeTable:delegate:useCommandLogic:
 * <LI>#activateMPN:coalescing:
 * <LI>#sendMessage:
 * <LI>#sendMessage:
 * </OL>
 * and the outcomes list may contain values like:<OL>
 * <LI>an <CODE>NSNull</CODE> (table subscriptions return immediately a valid table handle even inside a batch)
 * <LI>an LSMPNSubscription (MPN operations return their result only upon batch commit)
 * <LI>an <CODE>NSNull</CODE> (message requests return no result)
 * <LI>an LSPushServerException (in case the second #sendMessage: failed for some reason)
 * </OL>
 * To avoid handling result reordering, keep table requests and message requests in separate batches.
 * <BR/>This method is always blocking: it will return only after receiving the Server response for all the requests.
 *
 * @return The list of outcomes
 *
 * @throws LSPushClientException Thrown in case there is no current active batch.
 * @throws LSPushConnectionException Thrown if the client is not connected.
 */
- (NSArray *) commitBatch;

/**
 * Commits the current batch.
 * Non-exception-throwing variant of #commitBatch, see for more information.
 *
 * @param error In case of error, upon return contains an NSError object that describes the problem.
 *
 * @return The list of outcomes
 */
- (NSArray *) commitBatch:(NSError * __autoreleasing *)error;

/**
 * Aborts the current batch. Pending requests will be lost: any subscription handle or progressive number obtained from a request that is part of the ongoing batch
 * will have no value.
 */
- (void) abortBatch;


#pragma mark -
#pragma mark Properties

/**
 * Tells if the client is connected. If NO, any operation requiring an active connection will be refused.
 */
@property (nonatomic, readonly, getter= isConnected) BOOL connected;

/**
 * Tells if the client has an ongoing batch.
 */
@property (nonatomic, readonly, getter= isBatching) BOOL batching;

/**
 * Tells the current connection status.
 */
@property (nonatomic, readonly) LSConnectionStatus connectionStatus;

/**
 * Tells the current connection delegate.
 */
@property (nonatomic, weak, readonly) id <LSConnectionDelegate> delegate;


@end
