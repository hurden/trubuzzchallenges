//
//  LSMPNTokenStatus.h
//  Lightstreamer client for iOS
//

#ifndef Lightstreamer_client_for_iOS_LSMPNTokenStatus_h
#define Lightstreamer_client_for_iOS_LSMPNTokenStatus_h


/**
 * The LSMPNTokenStatus enum contains the possible statuses of the device token.
 * It is returned by the LSClient#registrationForMPNSucceededWithToken: method
 * after comparison with the previously known device token. It's
 * possible values are:<UL>
 * <LI>#LSMPNTokenStatusFirstUse: there was no previously known device token;
 * <LI>#LSMPNTokenStatusNotChanged: the previsouly known device token matches the current one;
 * <LI>#LSMPNTokenStatusChanged: the previously known device token differs from the current one.
 * </UL>
 * In the last case, a device token update will be tried on the Server. See
 * LSClient#registrationForMPNSucceededWithToken: for more information.
 */
typedef enum {
	LSMPNTokenStatusFirstUse= -1,
	LSMPNTokenStatusNotChanged= 0,
	LSMPNTokenStatusChanged= 1
} LSMPNTokenStatus;


#endif
