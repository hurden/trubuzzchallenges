//
//  LSException.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>


/**
 * The LSException class is the base class of all Lightstreamer client exceptions. It is never thrown.
 */
@interface LSException : NSException {}


#pragma mark -
#pragma mark Initialization

/**
 * Initializes an LSException object with specified parameters.
 * 
 * @param name Specific name of the exception.
 * @param reason Reason of the exception.
 *
 * @return The LSException object.
 */
- (id) initWithName:(NSString *)name reason:(NSString *)reason, ...;

/**
 * Initializes an LSException object with specified parameters.
 * 
 * @param name Specific name of the exception.
 * @param reason Reason of the exception.
 * @param arguments Variable argument list of parameters.
 *
 * @return The LSException object.
 */
- (id) initWithName:(NSString *)name reason:(NSString *)reason arguments:(va_list)arguments;


@end
