//
//  LSMPNTokenChangeInfo.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>


/**
 * The LSMPNTokenChangeInfo class contains additional information about the result of a device
 * token change on the Server (see LSConnectionDelegate#didSucceedChangingDeviceTokenOnServerWithInfo:).
 */
@interface LSMPNTokenChangeInfo : NSObject <NSCopying> {}


#pragma mark -
#pragma mark Initialization

/**
 * Creates and returns an LSMPNTokenChangeInfo object with the specified parameters.
 * <BR/>This factory method is for internal use only.
 *
 * @param restoredSubscriptions Number of the MPN subscriptions reactivated with the device token change.
 */
+ (LSMPNTokenChangeInfo *) mpnTokenChangeInfoWithRestoredSubscriptions:(int)restoredSubscriptions;

/**
 * Initializes an LSMPNTokenChangeInfo object with the specified parameters.
 * <BR/>This initializer is for internal use only.
 *
 * @param restoredSubscriptions Number of the MPN subscriptions reactivated with the device token change.
 */
- (id) initWithRestoredSubscriptions:(int)restoredSubscriptions;


#pragma mark -
#pragma mark Properties

/**
 * The number of MPN subscriptions reactivated with the device token change.
 */
@property (nonatomic, readonly) int restoredSubscriptions;


@end
