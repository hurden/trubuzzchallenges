//
//  LSLog.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>

#define LOG_SRC_CLIENT             (1)
#define LOG_SRC_STATE_MACHINE      (2)
#define LOG_SRC_SESSION            (4)
#define LOG_SRC_TIMER              (8)
#define LOG_SRC_URL_DISPATCHER    (16)
#define LOG_SRC_THREAD_POOL       (32)


@protocol LSLogDelegate;


/**
 * The LSLog class controls the local logging system.
 */
@interface LSLog : NSObject {}


#pragma mark -
#pragma mark Logging

/**
 * Logs a line if the source is enabled.
 * <BR/>This method is for internal use only.
 *
 * @param sourceType The identifier of the source.
 * @param source The logging source.
 * @param format The line format string.
 */
+ (void) sourceType:(int)sourceType source:(id)source log:(NSString *)format, ...;

/**
 * Logs a line.
 * <BR/>This method is for internal use only.
 *
 * @param format The line format string.
 */
+ (void) log:(NSString *)format, ...;


#pragma mark -
#pragma mark Log delegation

/**
 * Sets a new log delegate. Once a delegate has been set, all
 * subsequent log lines are redirected to the delegate. The
 * local logging system will just provide line formatting,
 * no log messages will be written to the console or other
 * destinations if not by delegate's initiative.
 *
 * @param delegate The log delegate, or nil to revert to 
 * the local logging system.
 */
+ (void) setDelegate:(id <LSLogDelegate>)delegate;


#pragma mark -
#pragma mark Source log filtering

/**
 * Enables logging for a specific source.
 *
 * @param source The identifier of the source.
 */
+ (void) enableSourceType:(int)source;

/**
 * Enables logging for all sources.
 */
+ (void) enableAllSourceTypes;

/**
 * Disables logging for a specific source.
 *
 * @param source The identifier of the source.
 */
+ (void) disableSourceType:(int)source;

/**
 * Disables logging for all sources.
 */
+ (void) disableAllSourceTypes;

/**
 * Tells is a specific source is enabled.
 *
 * @param source The identifier of the source.
 */
+ (BOOL) isSourceTypeEnabled:(int)source;


@end
