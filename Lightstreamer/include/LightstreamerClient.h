/*
 *  LightstreamerClient.h
 *  Lightstreamer client for iOS
 *
 */

#import "LSConnectionStatus.h"
#import "LSClient.h"
#import "LSLog.h"
#import "LSLogDelegate.h"
#import "LSMode.h"
#import "LSConnectionInfo.h"
#import "LSConnectionConstraints.h"
#import "LSSubscriptionConstraints.h"
#import "LSConnectionDelegate.h"
#import "LSException.h"
#import "LSPushClientException.h"
#import "LSPushConnectionException.h"
#import "LSPushServerException.h"
#import "LSPushUpdateException.h"
#import "LSUpdateInfo.h"
#import "LSTableInfo.h"
#import "LSExtendedTableInfo.h"
#import "LSTableDelegate.h"
#import "LSSubscribedTableKey.h"
#import "LSMPNTokenChangeInfo.h"
#import "LSMPNTokenStatus.h"
#import "LSMPNSubscriptionStatus.h"
#import "LSMPNDeviceStatus.h"
#import "LSMPNStatusInfo.h"
#import "LSMPNInfo.h"
#import "LSMPNKey.h"
#import "LSMPNSubscription.h"
#import "LSMessageInfo.h"
#import "LSMessageDelegate.h"

/**
 * @mainpage Getting started with the iOS and OS X Client libraries
 * 
 * The iOS and OS X Client Libraries have been developed using the Java SE library as a starting point.
 * Obtaining a connection and subscribing to a table is a simple and straightforward procedure:
 * <UL>
 *   <LI>Create an instance of LSClient</LI>
 *   <LI>Define an LSConnectionInfo object and open the connection</LI>
 *   <LI>When connected, define an LSTableInfo or LSExtendedTableInfo object
 *   <LI>Subscribe the table and start receiving the updates</LI>
 *   <LI>If needed, activate also an MPN subscription and receive the updates via mobile push (i.e. remote) notifications</LI>
 * </UL>
 *
 * @section create_lsclient Creating an instance of LSClient
 * To create an instance of LSClient simply alloc- and init-it, or use the factory method:
 * <BR/><BR/><CODE>LSClient *client= [[LSClient alloc] init];</CODE>
 * <BR/><BR/>Or:
 * <BR/><BR/><CODE>LSClient *client= [LSClient client];</CODE>
 *
 * @section open_con Open the connection
 * To open the connection you must first define the connection specifications using an LSConnectionInfo object,
 * for example:
 * <BR/><BR/><CODE>LSConnectionInfo *connectionInfo= [LSConnectionInfo connectionInfoWithPushServerURL:@@"http://push.lightstreamer.com" pushServerControlURL:nil user:nil password:nil adapter:@@"DEMO"];</CODE>
 * <BR/><BR/>Done this, use the LSConnectionInfo object with the LSClient:
 * <BR/><BR/><CODE>[client openConnectionWithInfo:connectionInfo delegate:self];</CODE>
 * <BR/><BR/>Please note that this call is <B>blocking</B>: it will not return until a connection has been established.
 * Since this may take long, you may want to run it from a background thread, to avoid freezing the UI.
 *
 * @section exception_vs_error Exceptions vs NSError
 * Client library APIs are designed to throw (raise) exceptions in case of both programming and runtime errors. Anyway, 
 * an alternative pattern is also provided that better fits Cocoa and Cocoa Touch.<BR/>
 * For every method that may throw an exception in presence of a runtime error, <B>a corresponding non-exception-throwing version is provided</B>. This alternative method
 * has always the same name, semantics, return value and parameters, except for an additional NSError output parameter that may be used (if not nil) to
 * store a runtime error. For example:
 * <BR/><BR/><CODE>@@try {<BR/>&nbsp;&nbsp;[client openConnectionWithInfo:connectionInfo delegate:self];<BR/>} @@catch (NSException *e) {<BR/>&nbsp;&nbsp;// Exception handling<BR/>}</CODE>
 * <BR/><BR/>May be replaced by:
 * <BR/><BR/><CODE>NSError *error= nil;<BR/>[client openConnectionWithInfo:connectionInfo delegate:self error:&error];<BR/>if (error) {<BR/>&nbsp;&nbsp;// Error handling<BR/>}</CODE>
 * <BR/><BR/>Note that exceptions due to programming errors (such as use of invalid field names, wrong API call sequence, etc.) may still be thrown.
 *
 * @section checking_con Listening for connection events
 * Your designated delegate will then receive an event when the session actually starts:
 * <BR/><BR/><CODE>- (void) clientConnection:(LSClient *)client didStartSessionWithPolling:(BOOL)polling {<BR/>&nbsp;&nbsp;&nbsp;&nbsp;...<BR/>}</CODE>
 * <BR/><BR/>Once this happens, you can start define an LSTableInfo object that describes the table you want to subscribe:
 * <BR/><BR/><CODE>LSTableInfo *tableInfo= [LSTableInfo tableInfoWithGroup:@@"item1 item2 item3" mode:LSModeMerge schema:@@"stock_name last_price min max" dataAdapter:@@"QUOTE_ADAPTER" snapshot:YES];</CODE>
 * <BR/><BR/>And then subscribe it using the LSClient:
 * <BR/><BR/><CODE>LSSubscribedTableKey *tableKey= [client subscribeTableWithExtendedInfo:tableInfo delegate:self useCommandLogic:NO];</CODE>
 * <BR/><BR/>If your code uses reference counting, remember to retain the obtained LSSubscribedTableKey object to later unsubscribe.
 * Normally, real-time subscription share the life cycle of the session, so they are closed when the session ends, but the library is able 
 * to automatically resubscribe in case of reconnection (see below <I>Automatic reconnections</I>).
 *
 * @section listen_table Listening for table events
 * Your designated table delegate will start to receive table updates as soon as they are sent from the Server:
 * <BR/><BR/><CODE>- (void) table:(LSSubscribedTableKey *)tableKey itemPosition:(int)itemPosition itemName:(NSString *)itemName didUpdateWithInfo:(LSUpdateInfo *)updateInfo {<BR/>&nbsp;&nbsp;&nbsp;&nbsp;...<BR/>}</CODE>
 * <BR/><BR/>Congratulations! Your real-time subscription with Lightstreamer Server is now set up!
 *
 * @section using_mpn Using Mobile Push Notifications subscriptions
 * An MPN subscription is backed by a real-time subscription, thus you need an LSTableInfo object describing the table you want to subscribe.
 * With MPN subscriptions there are some limitations, please check LSClient#activateMPN:coalescing: for more information.
 * <BR/>Once your LSTableInfo object is ready, you can create an LSMPNInfo object that includes the LSTableInfo and additionally describes how 
 * updates from the real-time subscription have to be delivered via mobile push (i.e. remote) notification:
 * <BR/><BR/><CODE>LSMPNInfo *mpnInfo= [LSMPNInfo mpnInfoWithTableInfo:tableInfo sound:@"Default" badge:@"AUTO" format:@"Stock ${stock_name} is now ${last_price}"];</CODE>
 * <BR/><BR/>You may then activate the MPN subscription:
 * <BR/><BR/><CODE>LSMPNSubscription *mpnSubscription= [client activateMPN:mpnInfo coalescing:NO];</CODE>
 * <BR/><BR/>The obtained LSMPNSubscription object contains a permanent, global, unique key of the MPN subscription: LSMPNSubscription#mpnKey.
 * It also provides services to obtain the status of the MPN subscription, modify it or deactivate it.
 * <BR/>Note that MPN subscriptions are persistent: they survive the session and to be deactivated you must do an explicit call
 * to LSMPNSubscription#deactivate, LSClient#deactivateAllMPNs or LSClient#deactivateMPNsWithStatus:.
 *
 * @section close_con Close the connection
 * If your code uses reference counting, remember to close the connection before releasing the LSClient (it will not be released, otherwise):
 * <BR/><BR/><CODE>[client closeConnection];</CODE>
 *
 * @section reconnection Automatic reconnections
 * The library has an advanced logic to automatically reconnect to the server in case of connection drops, timeouts, etc.
 * The network availability is constantly monitored to attempt a reconnection only when it can be successful, and at
 * the same time to preserve the device battery. At reconnection, the library will also automatically resubmit any subscription 
 * that was active when the connection dropped. This logic is active by default, and will stop trying only in presence of a
 * LSConnectionDelegate#clientConnection:didEndWithCause: event.
 * <BR/>In view of this, avoid to manually manage the reconnection unless you have a strong reason to do so.
 * If you really need to, close the connection during appropriate events to block the library's reconnection logic,
 * e.g.:
 * <BR/><BR/><CODE>- (void) clientConnection:(LSClient *)client didReceiveServerFailure:(LSPushServerException *)failure {<BR/>&nbsp;&nbsp;&nbsp;&nbsp;[client closeConnection];<BR/>}</CODE>
 * <BR/><BR/>Or:
 * <BR/><BR/><CODE>- (void) clientConnection:(LSClient *)client didReceiveConnectionFailure:(LSPushConnectionException *)failure {<BR/>&nbsp;&nbsp;&nbsp;&nbsp;[client closeConnection];<BR/>}</CODE>
 *
 * @section more_clients Using more than one LSClient
 * The library allows the use of more than one LSClient instance at the same time. However, consider that more clients means more threads, bigger memory footprint and more
 * overhead. Moreover, please recall that every operating system poses a hard limit on how many concurrent HTTP connections may be opened to the same end-point (i.e. host name
 * and port). So, if you are going to use <B>more than one LSClient to the same server</B> check the LSConnectionInfo#maxConcurrentSessionsPerServerExceededPolicy parameter.
 * With it, you can specify how the LSClient should behave in case this limit is exceeded. Finally, keep in mind that the library does not know
 * <I>a priori</I> this limit, it simply confronts the number of open connections to the same server with the static parameter LSConnectionInfo#maxConcurrentSessionsPerServer,
 * which is our best guess of this limit. Highering or loweing this parameter is possible although discouraged, and may lead to unexpected results.
 *
 */
