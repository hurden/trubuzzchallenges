//
//  LSExtendedTableInfo.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>
#import "LSTableInfo.h"


/**
 * The LSExtendedTableInfo class contains the specification of a table to be subscribed to Lightstreamer
 * Server, comprised all the item and field names. Group and Schema names to be sent to the Server are made by space
 * separated concatenations of item and field names. This name convention is suitable for a Metadata Adapter 
 * derived from LiteralBasedProvider or equivalent to it.
 * <BR/>Note that, since the LiteralBasedProvider extracts the list of items and fields by splitting the group ID
 * and schema name by spaces, item names and field names cannot contain spaces and must not be empty strings.
 */
@interface LSExtendedTableInfo : LSTableInfo <NSCopying> {}


#pragma mark -
#pragma mark Initialization

/**
 * Creates and returns an LSExtendedTableInfo object with the specified parameters.
 * <BR/>Note that item names and field names cannot contain spaces and must not be empty strings.
 *
 * @param items The list of strings with the items contained in this table.
 * @param mode The subscription mode for the items contained in this table.
 * @param fields The list of strings with the fields contained in this table.
 * @param dataAdapter The name of the Data Adapter (within the Adapter Set used by the current session) 
 * that supplies all the items in the Group. If it is nil, the "DEFAULT" name is used.
 * @param snapshot Whether or not the snapshot is being asked for the items contained in this table.
 *
 * @return The LSExtendedTableInfo object.
 *
 * @throws LSPushClientException If item names of field names are not valid.
 */
+ (LSExtendedTableInfo *) extendedTableInfoWithItems:(NSArray *)items mode:(LSMode)mode fields:(NSArray *)fields dataAdapter:(NSString *)dataAdapter snapshot:(BOOL)snapshot;


#pragma mark -
#pragma mark Properties

/**
 * The list of strings with the items contained in this table.
 * <BR/>Note that item names cannot contain spaces and must not be empty strings.
 *
 * @throws LSPushClientException While setting, if item names are not valid.
 */
@property (nonatomic, copy) NSArray *items;

/**
 * The list of strings with the fields contained in this table.
 * <BR/>Note that field names cannot contain spaces and must not be empty strings.
 *
 * @throws LSPushClientException While setting, if field names are not valid.
 */
@property (nonatomic, copy) NSArray *fields;


@end
